# CIS

Check systems against CIS benchmarks.

https://cisecurity.org

### Benchmark versions currently used
| Distribution               | Benchmark version |
| --                         | --                |
| Red Hat Enterprise Linux 7 | v3.0.1            |
| CentOS 7                   | v3.0.0            |
| Amazon Linux 2             | v1.0.0            |

## Pre-requisites

## Usage
Add the following to 'roles/requirements.yml' in your playbook directory
```
- name: cis
  src: https://gitlab.com/octo.tech/ansible/roles/cis.git
  scm: git
```

Then from your playbook directory install the role
```
ansible-galaxy install --roles-path roles/ -r roles/requirements.yml
```

Now you can add the role to your playbook file
```
- hosts: all
  roles:
     - cis
```

## License
MIT

## Bugs
If you have any problems using this role please raise an [Issue](https://gitlab.com/octo.tech/ansible/roles/cis/-/issues).

