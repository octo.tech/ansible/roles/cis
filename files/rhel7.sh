#!/bin/sh
# cis benchmarking checks
# based on document for RHEL7 (v3.0.1)
#
# author: Dudley Burrows <dudley@octo.tech>
#

section=""
results_doc="/tmp/$(date +%Y%m%d)-cis_results.csv"

# pretty colours
red="\e[31m"
green="\e[32m"
yellow="\e[1;33m"
blue="\e[34m"
grey="\e[1;30m"
reset="\e[0m"

# first things first, if not rhel7 we exit
grep "Red Hat.*release 7" /etc/redhat-release >/dev/null || exit 1

# script taken from page 194
ipv6=0
[ -n "$passing" ] && passing=""
[ -z "$(sudo grep "^\s*linux" /boot/grub2/grub.cfg \
			| grep -v ipv6.disable=1)" ] && passing="true"
sudo grep -Eq "^\s*net\.ipv6\.conf\.all\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" \
	/etc/sysctl.conf \
	/etc/sysctl.d/*.conf \
	&& sudo grep -Eq "^\s*net\.ipv6\.conf\.default\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" \
	/etc/sysctl.conf \
	/etc/sysctl.d/*.conf \
	&& sudo sysctl net.ipv6.conf.all.disable_ipv6 \
		| grep -Eq "^\s*net\.ipv6\.conf\.all\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" \
	&& sudo sysctl net.ipv6.conf.default.disable_ipv6 \
		| grep -Eq "^\s*net\.ipv6\.conf\.default\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" \
	&& passing="true"
if [ "$passing" = true ] ; then
	# IPv6 is disabled on the system
	ipv6=0
else
	# IPv6 is enabled on the system
	ipv6=1
fi

# ignore filesystems
# used for sections
#  - 1.1.22
#  - 6.1.10
#  - 6.1.11
#  - 6.1.12
#  - 6.1.13
#  - 6.1.14
ignorelocal="docker"

score() {
	_section=$1
	_result=$2
	_distro="RHEL"
	_date=$(date +%Y-%m-%d)
	_hostname=$(hostname -s)

	if [[ "${_result}" == "fail" ]] ; then
		_colour="${red}"
	elif [[ "${_result}" == "pass" ]] ; then
		_colour="${green}"
	elif [[ "${_result}" == "level2" ]] ; then
		_colour="${grey}"
	elif [[ "${_result}" == "manual" ]] ; then
		_colour="${yellow}"
	elif [[ "${_result}" == "excluded" ]] ; then
		_colour="${blue}"
	elif [[ "${_result}" == "n/a" ]] ; then
		_colour="${grey}"
	else
		_colour="${reset}"
	fi

	#echo -e "${_section}"
	echo -e "${_section}|${_colour}${_result}${reset}|${_hostname}|${_distro}|${_date}" >> ${results_doc}
}

module_chk() {
	_section="${1}"
	_fs="${2}"
	_modprobe_cmd=$(sudo which modprobe)
	_probe=$(sudo $_modprobe_cmd -n -v ${_fs} | grep -E "(${_fs}|install)")
	_lsmod=$(/sbin/lsmod | grep "${_fs}")

	if [[ $(echo ${_probe}) == "install /bin/true" ]] && [[ ${_lsmod} == "" ]] ; then
		score "${_section}" "pass"
	else
		score "${_section}" "fail"
	fi
}

fsopt_chk() {
	_section="${1}"
	_fs="${2}"
	_option="${3}"

	$(sudo mount | grep -E "\s/${_fs}\s" | grep -v ${_option} >/dev/null) \
		&& score "${_section}" "fail" \
		|| score "${_section}" "pass"
}

# exclusion set up
## certain systems have been excluded from one or more checks
## we need to mark these as excluded instead of failed

### check for docker
dockerhost=0
if systemctl cat -- docker.service &> /dev/null ; then
	(( dockerhost=dockerhost+1 ))
fi

## partition exclusions
fsmnt_chk() {
	_section="${1}"
	_fs="${2}"

	if mountpoint "${_fs}" &> /dev/null ; then
		$(sudo mount | grep -E "\s/${_fs}\s" >/dev/null) \
			&& score "${_section}" "pass" \
			|| score "${_section}" "fail"
	else
		score "${_section}" "excluded"
	fi
}

hostname

# start
echo "section|result" > ${results_doc}

## section 1

### section 1.1.1.1-4
### disable unused fs
#### we can exclude these
#module_chk "1.1.1.1 Ensure mounting of cramfs filesystems is disabled" "cramfs"
#module_chk "1.1.1.2 Ensure mounting of squashfs filesystems is disabled" "squashfs"
#module_chk "1.1.1.3 Ensure mounting of udf filesystems is disabled" "udf"
score "1.1.1.1 Ensure mounting of cramfs filesystems is disabled" "excluded"
score "1.1.1.2 Ensure mounting of squashfs filesystems is disabled" "excluded"
score "1.1.1.3 Ensure mounting of udf filesystems is disabled" "excluded"

score "1.1.1.4 Ensure mounting of FAT filesystems is limited" "excluded"

### section 1.1.2-5
### check /tmp
if $(sudo mount | grep -E '\s/tmp\s' >/dev/null) ; then
	score "1.1.2 Ensure /tmp is configured" "pass"
	fsopt_chk "1.1.3 Ensure noexec option set on /tmp partition" "tmp" "noexec"
	fsopt_chk "1.1.4 Ensure nodev option set on /tmp partition" "tmp" "nodev"
	fsopt_chk "1.1.5 Ensure nosuid option set on /tmp partition" "tmp" "nosuid"
else
	score "1.1.2 Ensure /tmp is configured" "excluded"
	score "1.1.3 Ensure noexec option set on /tmp partition" "excluded"
	score "1.1.4 Ensure nodev option set on /tmp partition" "excluded"
	score "1.1.5 Ensure nosuid option set on /tmp partition" "excluded"
fi

### section 1.1.6-9
### check /dev/shm
if $(sudo mount | grep -E '\s/dev/shm\s' >/dev/null) ; then
	score "1.1.6 Ensure /dev/shm is configured" "pass"
	fsopt_chk "1.1.7 Ensure noexec option set on /dev/shm partition" "dev/shm" "noexec"
	fsopt_chk "1.1.8 Ensure nodev option set on /dev/shm partition" "dev/shm" "nodev"
	fsopt_chk "1.1.9 Ensure nosuid option set on /dev/shm partition" "dev/shm" "nosuid"
else
	score "1.1.6 Ensure /dev/shm is configured" "excluded"
	score "1.1.7 Ensure noexec option set on /dev/shm partition" "excluded"
	score "1.1.8 Ensure nodev option set on /dev/shm partition" "excluded"
	score "1.1.9 Ensure nosuid option set on /dev/shm partition" "excluded"
fi

### section 1.1.10
### check /var
score "1.1.10 Ensure separate partition exists for /var" "excluded"

### section 1.1.11-14
### check /var/tmp
if $(sudo mount | grep -E '\s/var/tmp\s' >/dev/null) ; then
	score "1.1.11 Ensure separate partition exists for /var/tmp" "pass"
	fsopt_chk "1.1.12 Ensure noexec option set on /var/tmp partition" "var/tmp" "noexec"
	fsopt_chk "1.1.13 Ensure nodev option set on /var/tmp partition" "var/tmp" "nodev"
	fsopt_chk "1.1.14 Ensure nosuid option set on /var/tmp partition" "var/tmp" "nosuid"
else
	score "1.1.11 Ensure separate partition exists for /var/tmp" "excluded"
	score "1.1.12 Ensure noexec option set on /var/tmp partition" "excluded"
	score "1.1.13 Ensure nodev option set on /var/tmp partition" "excluded"
	score "1.1.14 Ensure nosuid option set on /var/tmp partition" "excluded"
fi

### section 1.1.15
### check /var/log
if $(sudo mount | grep -E '\s/var/log\s' >/dev/null) ; then
	score "1.1.15 Ensure separate partition exists for /var/log" "pass"
else
	score "1.1.15 Ensure separate partition exists for /var/log" "excluded"
fi

### section 1.1.16
### check /var/log/audit
if $(sudo mount | grep -E '\s/var/log/audit\s' >/dev/null) ; then
	score "1.1.16 Ensure separate partition exists for /var/log/audit" "pass"
else
	score "1.1.16 Ensure separate partition exists for /var/log/audit" "excluded"
fi

### section 1.1.17-18
### check /home
score "1.1.17 Ensure separate partition exists for /home" "excluded"
#if $(sudo mount | grep -E '\s/home\s' >/dev/null) ; then
#	fsopt_chk "1.1.18 Ensure nodev option set on /home partition" "home" "nodev"
#else
#	score "1.1.18 Ensure nodev option set on /home partition" "fail"
#fi
score "1.1.18 Ensure nodev option set on /home partition" "excluded"

### section 1.1.19-21
### check removable media
score "1.1.19 Ensure noexec option set on removable media/ partitions" "manual"
score "1.1.20 Ensure nodev option set on removable media/ partitions" "manual"
score "1.1.21 Ensure nosuid option set on removable media/ partitions" "manual"

### section 1.1.22
### check sticky world-writable dirs
[[ $(df --local -P 2> /dev/null \
	| grep -v -e "${ignorelocal}" \
	| awk '{if (NR!=1) print $6}' \
	| xargs -I '{}' \
	find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) \
	2>/dev/null) == "" ]] \
	&& score "1.1.22 Ensure sticky bit is set on all world-writable directories" "pass" \
	|| score "1.1.22 Ensure sticky bit is set on all world-writable directories" "fail"

### section 1.1.23
### disable automounting
[[ $(systemctl is-enabled autofs) =~ ^(disabled|masked)$ ]] \
	&& score "1.1.23 Disable Automounting" "pass" \
	|| score "1.1.23 Disable Automounting" "fail"

### section 1.1.24
### check usb-storage disabled
module_chk "1.1.24 Disable USB Storage" "usb-storage"

### section 1.2.1
### check gpg keys
# `rpm -q gpg-pubkey --qf '%{name}-%{version}-%{release} --> %{summary}\n'
# how do we verify these keys are correct?
# red hat key is here: https://access.redhat.com/security/team/key/
# do we just make sure there is a key for each repo?
# yum will not work if the key doesn't match.. right?
score "1.2.1 Ensure GPG keys are configured" "manual"

### section 1.2.2
### check repo
# `yum repolist`
# most systems have rhel and microsoft (for defender) repos
# a comparison list could be compiled for other systems
score "1.2.2 Ensure package manager repositories are configured" "manual"

### section 1.2.3
### ensure gpgpcheck activated
gpgcheck01=$(grep ^\s*gpgcheck /etc/yum.conf)
gpgcheck02=$(awk -v 'RS=[' -F '\n' '/\n\s*enabled\s*=\s*1(\W.*)?$/ \
				&& !/\n\s*gpgcheck\s*=\s*1(\W.*)?$/ \
				{ t=substr($1, 1, index($1, "]")-1); \
				print t,"does not have gpgcheck enabled." }' \
				/etc/yum.repos.d/*.repo | grep -v swiagent)
if [[ ${gpgcheck01} == "gpgcheck=1" ]] && [[ ${gpgcheck02} == "" ]] ; then
	score "1.2.3 Ensure gpgcheck is globally activated" "pass"
else
	score "1.2.3 Ensure gpgcheck is globally activated" "fail"
fi

### section 1.2.4
### check rhsm
sudo subscription-manager identity | grep "system\ identity" >/dev/null \
	&& score "1.2.4 Ensure Red Hat Subscription Manager connection is configured" "pass" \
	|| score "1.2.4 Ensure Red Hat Subscription Manager connection is configured" "fail"

### section 1.2.5
### check rhnsd daemon
[[ $(systemctl is-enabled rhnsd 2>/dev/null) == "enabled" ]] \
	&& score "1.2.5 Disable the rhnsd Daemon" "fail" \
	|| score "1.2.5 Disable the rhnsd Daemon" "pass"

### section 1.3.1
### check sudo installed
rpm -q sudo >/dev/null \
	&& score "1.3.1 Ensure sudo is installed" "pass" \
	|| score "1.3.1 Ensure sudo is installed" "fail"

### section 1.3.2
### sudo uses pty
#sudopty=$(sudo grep -Ei '^\s*Defaults\s+([^#]+(,s*|\s+))?use_pty\b' \
#			/etc/sudoers /etc/sudoers.d/* 2>/dev/null)
sudopty=$(sudo find /etc/sudoers /etc/sudoers.d/ -type f -exec \
	grep -Ei '^\s*Defaults\s+([^#]+(,s*|\s+))?use_pty\b' {} \;)
[[ ${sudopty} == "Defaults use_pty" ]] \
	&& score "1.3.2 Ensure sudo commands use pty" "pass" \
	|| score "1.3.2 Ensure sudo commands use pty" "fail"

### section 1.3.3
### sudo log file
#sudolog=$(sudo grep -Ei \
#			'^\s*Defaults\s+([^#;]+,\s*)?logfile\s*=\s*(")?[^#;]+(")?' \
#			/etc/sudoers /etc/sudoers.d/* 2>/dev/null)
sudolog=$(sudo find /etc/sudoers /etc/sudoers.d/ -type f -exec \
	grep -Ei '^\s*Defaults\s+([^#;]+,\s*)?logfile\s*=\s*(")?[^#;]+(")?' {} \;)
[[ ${sudolog} == 'Defaults logfile="/var/log/sudo.log"' ]] \
	&& score "1.3.3 Ensure sudo log file exists" "pass" \
	|| score "1.3.3 Ensure sudo log file exists" "fail"

### section 1.4.1
### aide installed
#rpm -q aide >/dev/null \
#	&& score "1.4.1 Ensure AIDE is installed" "pass" \
#	|| score "1.4.1 Ensure AIDE is installed" "fail"
score "1.4.1 Ensure AIDE is installed" "excluded"

### section 1.4.2
### filesystem integrity
#if sudo [ -f /var/spool/cron/root ] ; then
#	aidecheck01=$(sudo crontab -u root -l | grep aide)
#	aidecheck02=$(sudo grep -r aide /etc/cron.* /etc/crontab)
#	if [[ ${aidecheck01} != "" || ${aidecheck02} != "" ]] ; then
#		score "1.4.2 Ensure filesystem integrity is regularly checked" "pass"
#	else
#		score "1.4.2 Ensure filesystem integrity is regularly checked" "fail"
#	fi
#else
#	score "1.4.2 Ensure filesystem integrity is regularly checked" "fail"
#fi
score "1.4.2 Ensure filesystem integrity is regularly checked" "excluded"

### section 1.5.1
### bootloader password
if sudo [ -f /boot/grub2/user.cfg ] ; then
	sudo grep "^\s*GRUB2_PASSWORD" /boot/grub2/user.cfg >/dev/null \
		&& score "1.5.1 Ensure bootloader password is set" "pass" \
		|| score "1.5.1 Ensure bootloader password is set" "fail"
elif sudo [ -f /boot/grub2/grub.cfg ] ; then
	grubcheck01=$(sudo grep "^\s*set superusers" /boot/grub2/grub.cfg)
	grubcheck02=$(sudo grep "^\s*password" /boot/grub2/grub.cfg)
	[[ ${grubcheck01} || ${grubcheck02} ]] \
		&& score "1.5.1 Ensure bootloader password is set" "pass" \
		|| score "1.5.1 Ensure bootloader password is set" "fail"
else
	score "1.5.1 Ensure bootloader password is set" "fail"
fi

### section 1.5.2
### bootloader config permissions
if sudo [ -f /boot/grub2/user.cfg ] ; then
	sudo stat /boot/grub2/user.cfg \
	| grep "Access: (0600/-rw-------)  Uid: (    0/    root)   Gid: (    0/    root)" >/dev/null \
	&& score "1.5.2 Ensure permissions on bootloader config are configured" "pass" \
	|| score "1.5.2 Ensure permissions on bootloader config are configured" "fail"
elif sudo [ -f /boot/grub2/grub.cfg ] ; then
	sudo stat /boot/grub2/grub.cfg \
	| grep "Access: (0600/-rw-------)  Uid: (    0/    root)   Gid: (    0/    root)" >/dev/null \
	&& score "1.5.2 Ensure permissions on bootloader config are configured" "pass" \
	|| score "1.5.2 Ensure permissions on bootloader config are configured" "fail"
else
	score "1.5.2 Ensure permissions on bootloader config are configured" "fail"
fi

### section 1.5.3
### auth for single user mode
singleusercheck01=$(sudo grep /sbin/sulogin \
						/usr/lib/systemd/system/rescue.service)
singleusercheck02=$(sudo grep /sbin/sulogin \
						/usr/lib/systemd/system/emergency.service)
[[ ${singleusercheck01} && ${singleusercheck02} ]] \
	&& score "1.5.3 Ensure authentication required for single user mode" "pass" \
	|| score "1.5.3 Ensure authentication required for single user mode" "fail"

### section 1.6.1
### core dumps are restricted
#coredumpscore=0
#[[ $(sudo grep -E "^\s*\*\s+hard\s+core" \
#	/etc/security/limits.conf \
#	/etc/security/limits.d/* \
#	>/dev/null) == "* hard core 0" ]] \
#	&& (( coredumpscore=coredumpscore+1 ))
#
#[[ $(sudo sysctl fs.suid_dumpable) == "fs.suid_dumpable = 0" ]] \
#	&& (( coredumpscore=coredumpscore+1 ))
#
#[[ $(sudo grep "fs\.suid_dumpable" \
#	/etc/sysctl.conf \
#	/etc/sysctl.d/*) == "fs.suid_dumpable = 0" ]] \
#	&& (( coredumpscore=coredumpscore+1 ))
#
#[[ ${coredumpscore} -eq 3 ]] \
#	&& score "1.6.1 Ensure core dumps are restricted" "pass" \
#	|| score "1.6.1 Ensure core dumps are restricted" "fail"
score "1.6.1 Ensure core dumps are restricted" "excluded"

### section 1.6.2
### xd/nx support enabled
#if which journalctl >/dev/null ; then
#	[[ $(journalctl | grep 'protection: active') \
#	== "kernel: NX (Execute Disable) protection: active" ]] \
#	&& score "1.6.2 Ensure XD/NX support is enabled" "pass" \
#	|| score "1.6.2 Ensure XD/NX support is enabled" "fail"
#else
#	[[ -n $(grep noexec[0-9]*=off /proc/cmdline) \
#		|| -z $(grep -E -i ' (pae|nx)' /proc/cpuinfo) \
#		|| -n $(grep '\sNX\s.*\sprotection:\s' /var/log/dmesg \
#		| grep -v active) ]] \
#		&& score "1.6.2 Ensure XD/NX support is enabled" "fail" \
#		|| score "1.6.2 Ensure XD/NX support is enabled" "pass"
#fi
score "1.6.2 Ensure XD/NX support is enabled" "excluded"

### section 1.6.3
### aslr enabled
#aslrcheck01=$(sudo sysctl kernel.randomize_va_space)
#aslrcheck02=$(sudo grep "kernel\.randomize_va_space" \
#				/etc/sysctl.conf /etc/sysctl.d/*)
#[[ ${aslrcheck01} && ${aslrcheck02} == "kernel.randomize_va_space = 2" ]] \
#	&& score "1.6.3 Ensure address space layout randomization (ASLR) is enabled" "pass" \
#	|| score "1.6.3 Ensure address space layout randomization (ASLR) is enabled" "fail"
score "1.6.3 Ensure address space layout randomization (ASLR) is enabled" "excluded"

### section 1.6.4
### prelink disabled
rpm -q prelink >/dev/null \
	&& score "1.6.4 Ensure prelink is disabled" "fail" \
	|| score "1.6.4 Ensure prelink is disabled" "pass"

### section 1.7.1.1
### selinux installed
rpm -q libselinux >/dev/null \
	&& score "1.7.1.1 Ensure SELinux is installed" "pass" \
	|| score "1.7.1.1 Ensure SELinux is installed" "fail"

### section 1.7.1.2
### selinux not disabled in bootloader
grubconf=""
sudo [ -f /boot/grub2/user.cfg ] && grubconf="/boot/grub2/user.cfg"
sudo [ -f /boot/grub2/grub.cfg ] && grubconf="/boot/grub2/grub.cfg"

sudo grep "^\s*linux" ${grubconf} | grep -E "(selinux=0|enforcing=0)" \
	&& score "1.7.1.2 Ensure SELinux is not disabled in bootloader configuration" "fail" \
	|| score "1.7.1.2 Ensure SELinux is not disabled in bootloader configuration" "pass"

### section 1.7.1.3
### selinux policy configured
#[[ $(grep -E "SELINUXTYPE=targeted" /etc/selinux/config) \
#	&& $(/usr/sbin/sestatus | grep -E 'Loaded policy.*targeted') ]] \
#	&& score "1.7.1.3 Ensure SELinux policy is configured" "pass" \
#	|| score "1.7.1.3 Ensure SELinux policy is configured" "fail"
score "1.7.1.3 Ensure SELinux policy is configured" "excluded"

### section 1.7.1.4
### selinux in enforcing or permissive
[[ $(/usr/sbin/getenforce) == "Enforcing" || "Permissive" ]] \
	&& [[ $(grep -Ei '^\s*SELINUX=(enforcing|permissive)' \
			/etc/selinux/config) \
			== "SELINUX=enforcing" || "SELINUX=permissive" ]] \
			&& score "1.7.1.4 Ensure the SELinux mode is enforcing or permissive" "pass" \
			|| score "1.7.1.4 Ensure the SELinux mode is enforcing or permissive" "fail"

### section 1.7.1.5
### selinux enforcing
score "1.7.1.5 Ensure the SELinux mode is enforcing" "excluded"

### section 1.7.1.6
### no unconfined services
ps -eZ | grep unconfined_service_t \
	&& score "1.7.1.6 Ensure no unconfined services exist" "fail" \
	|| score "1.7.1.6 Ensure no unconfined services exist" "pass"

### section 1.7.1.7
### setroubleshoot not installed
rpm -q setroubleshoot >/dev/null \
	&& score "1.7.1.7 Ensure SETroubleshoot is not installed" "fail" \
	|| score "1.7.1.7 Ensure SETroubleshoot is not installed" "pass"

### section 1.7.1.8
### mcstrans not installed
rpm -q mcstrans >/dev/null \
	&& score "1.7.1.8 Ensure the MCS Translation Service (mcstrans) is not installed" "fail" \
	|| score "1.7.1.8 Ensure the MCS Translation Service (mcstrans) is not installed" "pass"

### section 1.8.1.1
### motd configured properly
#grep -Ei "(\\\v|\\\r|\\\m|\\\s|\
#	$(grep '^ID=' /etc/os-release \
#	| cut -d= -f2 \
#	| sed -e 's/"//g')) >/dev/null" \
#	/etc/motd \
#	&& score "1.8.1.1 Ensure message of the day is configured properly" "fail" \
#	|| score "1.8.1.1 Ensure message of the day is configured properly" "pass"
score "1.8.1.1 Ensure message of the day is configured properly" "excluded"

### section 1.8.1.2
### local login warning banner configured properly
grep -Ei "(\\\v|\\\r|\\\m|\\\s|\
	$(grep '^ID=' /etc/os-release \
	| cut -d= -f2 \
	| sed -e 's/"//g')) >/dev/null" \
	/etc/issue \
	&& score "1.8.1.2 Ensure local login warning banner is configured properly" "fail" \
	|| score "1.8.1.2 Ensure local login warning banner is configured properly" "pass"

### section 1.8.1.3
### local login warning banner configured properly
grep -Ei "(\\\v|\\\r|\\\m|\\\s|\
	$(grep '^ID=' /etc/os-release \
	| cut -d= -f2 \
	| sed -e 's/"//g')) >/dev/null" \
	/etc/issue.net \
	&& score "1.8.1.3 Ensure remote login warning banner is configured properly" "fail" \
	|| score "1.8.1.3 Ensure remote login warning banner is configured properly" "pass"

### section 1.8.1.4
### motd permissions
#sudo stat /etc/motd \
#	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
#	>/dev/null \
#	&& score "1.8.1.4 Ensure permissions on /etc/motd are configured" "pass" \
#	|| score "1.8.1.4 Ensure permissions on /etc/motd are configured" "fail"
score "1.8.1.4 Ensure permissions on /etc/motd are configured" "excluded"

### section 1.8.1.5
### motd permissions
sudo stat /etc/issue \
	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "1.8.1.5 Ensure permissions on /etc/issue are configured" "pass" \
	|| score "1.8.1.5 Ensure permissions on /etc/issue are configured" "fail"

### section 1.8.1.6
### motd permissions
sudo stat /etc/issue.net \
	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "1.8.1.6 Ensure permissions on /etc/issue.net are configured" "pass" \
	|| score "1.8.1.6 Ensure permissions on /etc/issue.net are configured" "fail"

### section 1.9
### security patches applied
sudo yum check-update --security >/dev/null \
	&& score "1.9 Ensure updates, patches, and additional security software are installed" "pass" \
	|| score "1.9 Ensure updates, patches, and additional security software are installed" "fail"

### section 1.10
### gdm removed (or login configured - not checked)
rpm -q gdm >/dev/null \
	&& score "1.10 Ensure GDM is removed or login is configured" "fail" \
	|| score "1.10 Ensure GDM is removed or login is configured" "pass"

## section 2

### section 2.1.2
### xinetd not installed
rpm -q xinetd >/dev/null \
	&& score "2.1.2 Ensure xinetd is not installed" "fail" \
	|| score "2.1.2 Ensure xinetd is not installed" "pass"

### section 2.2.1.1
### time sync in use
if rpm -q chrony >/dev/null || rpm -q ntp >/dev/null ; then
	score "2.2.1.1 Ensure time synchronization is in use" "pass"
else
	score "2.2.1.1 Ensure time synchronization is in use" "fail"
fi

### section 2.2.1.2
### chrony config
if rpm -q chrony >/dev/null ; then
	if [[ $(grep -E "^(server|pool)" /etc/chrony.conf >/dev/null) \
			&& $(stat /etc/sysconfig/chronyd &>/dev/null) ]] \
	&& [[ $(grep ^OPTIONS /etc/sysconfig/chronyd) == 'OPTIONS="-u chrony"' ]]
	then
		score "2.2.1.2 Ensure chrony is configured" "pass"
	else
		score "2.2.1.2 Ensure chrony is configured" "fail"
	fi
else
	score "2.2.1.2 Ensure chrony is configured" "n/a"
fi

### section 2.2.1.3
### ntp config
if rpm -q ntp >/dev/null ; then
	if grep -P "^restrict.*default (?=.*?kod)(?=.*?nomodify)(?=.*?notrap)(?=.*?nopeer)(?=.*?noquery)" /etc/ntp.conf >/dev/null \
        && grep -E "^(server|pool)" /etc/ntp.conf >/dev/null \
        && [[ $(grep "^OPTIONS" /etc/sysconfig/ntpd) == 'OPTIONS="-u ntp:ntp"' \
		|| $(grep "^ExecStart" /usr/lib/systemd/system/ntpd.service) \
				== 'ExecStart=/usr/sbin/ntpd -u ntp:ntp $OPTIONS' ]]
	then
		score "2.2.1.3 Ensure ntp is configured" "pass"
	else
		score "2.2.1.3 Ensure ntp is configured" "fail"
	fi
else
	score "2.2.1.3 Ensure ntp is configured" "n/a"
fi

### section 2.2.2
### x11 not installed
[[ $(rpm -qa xorg-x11-server*) == "" ]] \
	&& score "2.2.2 Ensure X11 Server components are not installed" "pass" \
	|| score "2.2.2 Ensure X11 Server components are not installed" "fail"

### section 2.2.3
### avahi not installed
rpm -q avahi-autoipd avahi >/dev/null \
	&& score "2.2.3 Ensure Avahi Server is not installed" "fail" \
	|| score "2.2.3 Ensure Avahi Server is not installed" "pass"

### section 2.2.4
### cups not installed
rpm -q cups >/dev/null \
	&& score "2.2.4 Ensure CUPS is not installed" "fail" \
	|| score "2.2.4 Ensure CUPS is not installed" "pass"

### section 2.2.5
### dhcp not installed
rpm -q dhcp >/dev/null \
	&& score "2.2.5 Ensure DHCP Server is not installed" "fail" \
	|| score "2.2.5 Ensure DHCP Server is not installed" "pass"

### section 2.2.6
### ldap server not installed
rpm -q openldap-servers >/dev/null \
	&& score "2.2.6 Ensure LDAP server is not installed" "fail" \
	|| score "2.2.6 Ensure LDAP server is not installed" "pass"

### section 2.2.7
### nfs-utils not installed
### or nfs-server is masked
#if rpm -q nfs-utils >/dev/null; then
#	if grep nfs /proc/mounts >/dev/null ; then
#		systemctl is-enabled nfs-server \
#			| grep -E "disabled|masked" >/dev/null \
#			&& score "2.2.7 Ensure nfs-utils is not installed or the nfs-server service is masked" "pass" \
#			|| score "2.2.7 Ensure nfs-utils is not installed or the nfs-server service is masked" "fail"
#	else
#		score "2.2.7 Ensure nfs-utils is not installed or the nfs-server service is masked" "fail"
#	fi
#else
#	score "2.2.7 Ensure nfs-utils is not installed or the nfs-server service is masked" "fail"
#fi
score "2.2.7 Ensure nfs-utils is not installed or the nfs-server service is masked" "excluded"

### section 2.2.8
### rpcbind not installed
### rpcbind is masked
#if rpm -q rpcbind >/dev/null ; then
#	if grep nfs /proc/mounts >/dev/null ; then
#		systemctl is-enabled rpcbind{,.socket} \
#			| grep -E "disabled|masked" >/dev/null \
#			&& score "2.2.8 Ensure rpcbind is not installed or the rpcbind services are masked" "pass" \
#			|| score "2.2.8 Ensure rpcbind is not installed or the rpcbind services are masked" "fail"
#	else
#		score "2.2.8 Ensure rpcbind is not installed or the rpcbind services are masked" "fail"
#	fi
#else
#	score "2.2.8 Ensure rpcbind is not installed or the rpcbind services are masked" "fail"
#fi
score "2.2.8 Ensure rpcbind is not installed or the rpcbind services are masked" "excluded"

### section 2.2.9
### dns not installed
rpm -q bind >/dev/null \
	&& score "2.2.9 Ensure DNS Server is not installed" "fail" \
	|| score "2.2.9 Ensure DNS Server is not installed" "pass"

### section 2.2.10
### ftp not installed
rpm -q vsftpd >/dev/null \
	&& score "2.2.10 Ensure FTP Server is not installed" "fail" \
	|| score "2.2.10 Ensure FTP Server is not installed" "pass"

### section 2.2.11
### httpd not installed
if ! systemctl is-active httpd.service &> /dev/null ; then
	rpm -q httpd >/dev/null \
		&& score "2.2.11 Ensure HTTP server is not installed" "fail" \
		|| score "2.2.11 Ensure HTTP server is not installed" "pass"
else
	score "2.2.11 Ensure HTTP server is not installed" "excluded"
fi

### section 2.2.12
### dovecot not installed
rpm -q dovecot >/dev/null \
	&& score "2.2.12 Ensure IMAP and POP3 server is not installed" "fail" \
	|| score "2.2.12 Ensure IMAP and POP3 server is not installed" "pass"

### section 2.2.13
### samba not installed
if ! systemctl is-active smb.service &> /dev/null ; then
	rpm -q samba >/dev/null \
		&& score "2.2.13 Ensure Samba is not installed" "fail" \
		|| score "2.2.13 Ensure Samba is not installed" "pass"
else
	score "2.2.13 Ensure Samba is not installed" "excluded"
fi

### section 2.2.14
### http proxy not installed
rpm -q squid >/dev/null \
	&& score "2.2.14 Ensure HTTP Proxy Server is not installed" "fail" \
	|| score "2.2.14 Ensure HTTP Proxy Server is not installed" "pass"

### section 2.2.15
### net-snmp not installed
#rpm -q net-snmp >/dev/null \
#	&& score "2.2.15 Ensure net-snmp is not installed" "fail" \
#	|| score "2.2.15 Ensure net-snmp is not installed" "pass"
## this requirement should be phased out with orion agent
score "2.2.15 Ensure net-snmp is not installed" "excluded"

### section 2.2.16
### mta configured local-only
sudo ss -lntu | grep -E ':25\s' | grep -E -v '\s(127.0.0.1|\[?::1\]?):25\s' >/dev/null \
	&& score "2.2.16 Ensure mail transfer agent is configured for local-only mode" "fail" \
	|| score "2.2.16 Ensure mail transfer agent is configured for local-only mode" "pass"

### section 2.2.17
### rsync not installed
### or rsyncd is masked
#if rpm -q rsync >/dev/null ; then
#	systemctl is-enabled rsyncd \
#		| grep -E "disabled|masked" >/dev/null \
#		&& score "2.2.17 Ensure rsync is not installed or the rsyncd service is masked" "pass" \
#		|| score "2.2.17 Ensure rsync is not installed or the rsyncd service is masked" "fail"
#else
#	score "2.2.17 Ensure rsync is not installed or the rsyncd service is masked" "pass"
#fi
score "2.2.17 Ensure rsync is not installed or the rsyncd service is masked" "excluded"

### section 2.2.18
### nis server not installed
rpm -q ypserv >/dev/null \
	&& score "2.2.18 Ensure NIS server is not installed" "fail" \
	|| score "2.2.18 Ensure NIS server is not installed" "pass"

### section 2.2.19
### telnet-server not installed
rpm -q telnet-server >/dev/null \
	&& score "2.2.19 Ensure telnet-server is not installed" "fail" \
	|| score "2.2.19 Ensure telnet-server is not installed" "pass"

### section 2.3.1
### nis client not installed
rpm -q ypbind >/dev/null \
	&& score "2.3.1 Ensure NIS Client is not installed" "fail" \
	|| score "2.3.1 Ensure NIS Client is not installed" "pass"

### section 2.3.2
### rsh client not installed
rpm -q rsh >/dev/null \
	&& score "2.3.2 Ensure rsh client is not installed" "fail" \
	|| score "2.3.2 Ensure rsh client is not installed" "pass"

### section 2.3.3
### talk client not installed
rpm -q talk >/dev/null \
	&& score "2.3.3 Ensure talk client is not installed" "fail" \
	|| score "2.3.3 Ensure talk client is not installed" "pass"

### section 2.3.4
### telnet client not installed
case $(hostname -s) in
	*netmon*) score "2.3.4 Ensure telnet client is not installed" "excluded" ;;
	*) rpm -q telnet >/dev/null \
		&& score "2.3.4 Ensure telnet client is not installed" "fail" \
		|| score "2.3.4 Ensure telnet client is not installed" "pass" ;;
esac

### section 2.3.5
### ldap client not installed
rpm -q openldap-clients >/dev/null \
	&& score "2.3.5 Ensure LDAP client is not installed" "fail" \
	|| score "2.3.5 Ensure LDAP client is not installed" "pass"

### section 2.5
### nonessential services {removed,masked}
###lsof -i -P -n | grep -v "(ESTABLISHED)"
score "2.5 Ensure nonessential services are removed or masked" "manual"

## section 3

### section 3.1.1
### disable ipv6
[[ ${ipv6} == 0 ]] \
	&& score "3.1.1 Disable IPv6" "pass" \
	|| score "3.1.1 Disable IPv6" "fail" \

### section 3.1.2
### disable wireless interfaces
sudo iw list &>/dev/null \
	&& score "3.1.2 Ensure wireless interfaces are disabled" "fail" \
	|| score "3.1.2 Ensure wireless interfaces are disabled" "pass"

### section 3.2.1
### ip forwarding disabled
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.ip_forward) == "net.ipv4.ip_forward = 0" \
		&& $(sudo grep -E -s "^\s*net\.ipv4\.ip_forward\s*=\s*1" \
				/etc/sysctl.conf \
				/etc/sysctl.d/*.conf \
				/usr/lib/sysctl.d/*.conf \
				/run/sysctl.d/*.conf >/dev/null) == "" ]] \
				&& score "3.2.1 Ensure IP forwarding is disabled" "pass" \
				|| score "3.2.1 Ensure IP forwarding is disabled" "fail"
else
	score "3.2.1 Ensure IP forwarding is disabled" "excluded"
fi

# if ipv6 is enabled
#sysctl net.ipv6.conf.all.forwarding == "net.ipv6.conf.all.forwarding = 0"
#grep -E -s "^\s*net\.ipv6\.conf\.all\.forwarding\s*=\s*1" \
#	/etc/sysctl.conf \
#	/etc/sysctl.d/*.conf \
#	/usr/lib/sysctl.d/*.conf \
#	/run/sysctl.d/*.conf

### section 3.2.2
### packet redirect disabled
if [[ "$dockerhost" -eq 0 ]] ; then
	SEND_REDIRECTS=true
	[[ $(sudo sysctl net.ipv4.conf.all.send_redirects) \
		== "net.ipv4.conf.all.send_redirects = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv4.conf.default.send_redirects) \
		== "net.ipv4.conf.default.send_redirects = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep -h "net\.ipv4\.conf\.all\.send_redirects" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep -h "net\.ipv4\.conf\.default\.send_redirects" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.2.2 Ensure packet redirect sending is disabled" "pass" \
		|| score "3.2.2 Ensure packet redirect sending is disabled" "fail"
else
	score "3.2.2 Ensure packet redirect sending is disabled" "excluded"
fi

### section 3.3.1
### source routed packets not accepted
if [[ "$dockerhost" -eq 0 ]] ; then
	SEND_REDIRECTS=true
	[[ $(sudo sysctl net.ipv4.conf.all.accept_source_route) \
		== "net.ipv4.conf.all.accept_source_route = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv4.conf.default.accept_source_route) \
		== "net.ipv4.conf.default.accept_source_route = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.all\.accept_source_route" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.default\.accept_source_route" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.1 Ensure source routed packets are not accepted" "pass" \
		|| score "3.3.1 Ensure source routed packets are not accepted" "fail"
else
	score "3.3.1 Ensure source routed packets are not accepted" "excluded"
fi

#[[ $(sudo sysctl net.ipv6.conf.all.accept_source_route) \
#	== "net.ipv6.conf.all.accept_source_route = 0" \
#	&& $(sudo sysctl net.ipv6.conf.default.accept_source_route) \
#	== "net.ipv6.conf.default.accept_source_route = 0" \
#	&& $(grep "net\.ipv6\.conf\.all\.accept_source_route" \
#			/etc/sysctl.conf /etc/sysctl.d/*) \
#	== "net.ipv6.conf.all.accept_source_route = 0" \
#	&& $(grep "net\.ipv6\.conf\.all\.accept_source_route" \
#			/etc/sysctl.conf /etc/sysctl.d/*) \
#	== "net.ipv6.conf.all.accept_source_route = 0" ]] \
#	&& score "3.3.1" "pass" \
#	|| score "3.3.1" "fail"
### script taken from page 201
#[ -n "$passing" ] && passing=""
#[ -z "$(grep "^\s*linux" /boot/grub2/grub.cfg | grep -v ipv6.disable=1)" ] && 
#passing="true" 
#grep -Eq "^\s*net\.ipv6\.conf\.all\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" 
#/etc/sysctl.conf \ 
#/etc/sysctl.d/*.conf && grep -Eq 
#"^\s*net\.ipv6\.conf\.default\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" \ 
#/etc/sysctl.conf /etc/sysctl.d/*.conf && sysctl 
#net.ipv6.conf.all.disable_ipv6 | \ 
#grep -Eq "^\s*net\.ipv6\.conf\.all\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" && \ 
#sysctl net.ipv6.conf.default.disable_ipv6 | \ 
#grep -Eq "^\s*net\.ipv6\.conf\.default\.disable_ipv6\s*=\s*1\b(\s+#.*)?$" && 
#passing="true" 
#if [ "$passing" = true ] ; then 
# 
#echo "IPv6 is disabled on the system" 
#else 
#echo "IPv6 is enabled on the system" 
#fi

### section 3.3.2
### icmp redirects not accepted
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.conf.all.accept_redirects) \
		== "net.ipv4.conf.all.accept_redirects = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv4.conf.default.accept_redirects) \
		== "net.ipv4.conf.default.accept_redirects = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.all\.accept_redirects" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.default\.accept_redirects" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.2 Ensure ICMP redirects are not accepted" "pass" \
		|| score "3.3.2 Ensure ICMP redirects are not accepted" "fail"
else
	score "3.3.2 Ensure ICMP redirects are not accepted" "excluded"
fi

### section 3.3.3
### secure icmp redirects not accepted
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.conf.all.secure_redirects) \
		== "net.ipv4.conf.all.secure_redirects = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv4.conf.default.secure_redirects) \
		== "net.ipv4.conf.default.secure_redirects = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.all\.secure_redirects" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.default\.secure_redirects" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.3 Ensure secure ICMP redirects are not accepted" "pass" \
		|| score "3.3.3 Ensure secure ICMP redirects are not accepted" "fail"
else
	score "3.3.3 Ensure secure ICMP redirects are not accepted" "excluded"
fi

### section 3.3.4
### suspicious packets logged
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.conf.all.log_martians) \
		== "net.ipv4.conf.all.log_martians = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv4.conf.default.log_martians) \
		== "net.ipv4.conf.default.log_martians = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.all\.log_martians" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.all\.log_martians" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.4 Ensure suspicious packets are logged" "pass" \
		|| score "3.3.4 Ensure suspicious packets are logged" "fail"
else
	score "3.3.4 Ensure suspicious packets are logged" "excluded"
fi

### section 3.3.5
### broadcast icmp requests ignored
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.icmp_echo_ignore_broadcasts) \
		== "net.ipv4.icmp_echo_ignore_broadcasts = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.icmp_echo_ignore_broadcasts" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.5 Ensure broadcast ICMP requests are ignored" "pass" \
		|| score "3.3.5 Ensure broadcast ICMP requests are ignored" "fail"
else
	score "3.3.5 Ensure broadcast ICMP requests are ignored" "excluded"
fi

### section 3.3.6
### bogus icmp responses ignored
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.icmp_ignore_bogus_error_responses) \
		== "net.ipv4.icmp_ignore_bogus_error_responses = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.icmp_ignore_bogus_error_responses" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.6 Ensure bogus ICMP responses are ignored" "pass" \
		|| score "3.3.6 Ensure bogus ICMP responses are ignored" "fail"
else
	score "3.3.6 Ensure bogus ICMP responses are ignored" "excluded"
fi

### section 3.3.7
### reverse path filtering enabled
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.conf.all.rp_filter) \
		== "net.ipv4.conf.all.rp_filter = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv4.conf.default.rp_filter) \
		== "net.ipv4.conf.default.rp_filter = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.all\.rp_filter" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.conf\.default\.rp_filter" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.7 Ensure Reverse Path Filtering is enabled" "pass" \
		|| score "3.3.7 Ensure Reverse Path Filtering is enabled" "fail"
else
	score "3.3.7 Ensure Reverse Path Filtering is enabled" "excluded"
fi

### section 3.3.8
### tcp syn cookies enabled
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv4.tcp_syncookies) \
		== "net.ipv4.tcp_syncookies = 1" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv4\.tcp_syncookies" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.8 Ensure TCP SYN Cookies is enabled" "pass" \
		|| score "3.3.8 Ensure TCP SYN Cookies is enabled" "fail"
else
	score "3.3.8 Ensure TCP SYN Cookies is enabled" "excluded"
fi

### section 3.3.9
### ipv6 router adverts not accepted
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo sysctl net.ipv6.conf.all.accept_ra) \
		== "net.ipv6.conf.all.accept_ra = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(sudo sysctl net.ipv6.conf.default.accept_ra) \
		== "net.ipv6.conf.default.accept_ra = 0" ]] \
			|| SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv6\.conf\.all\.accept_ra" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	[[ $(grep "net\.ipv6\.conf\.default\.accept_ra" \
				/etc/sysctl.conf /etc/sysctl.d/* | wc -l) \
		-gt 0 ]] || SEND_REDIRECTS=${SEND_REDIRECTS:+false}
	
	[[ ${SEND_REDIRECTS} == "true" ]] \
		&& score "3.3.9 Ensure IPv6 router advertisements are not accepted" "pass" \
		|| score "3.3.9 Ensure IPv6 router advertisements are not accepted" "fail"
else
	score "3.3.9 Ensure IPv6 router advertisements are not accepted" "excluded"
fi

## ipv6 disabled check

### section 3.4.1
### dccp disabled
module_chk "3.4.1 Ensure DCCP is disabled" "dccp"

### section 3.4.2
### sctp disabled
module_chk "3.4.2 Ensure SCTP is disabled" "sctp"

### section 3.5.1.1
### firewalld installed
rpm -q firewalld iptables >/dev/null \
	&& score "3.5.1.1 Ensure FirewallD is installed" "pass" \
	|| score "3.5.1.1 Ensure FirewallD is installed" "fail"

### section 3.5.1.2
### iptables-services not installed
rpm -q iptables-services >/dev/null \
	&& score "3.5.1.2 Ensure iptables-services package is not installed" "fail" \
	|| score "3.5.1.2 Ensure iptables-services package is not installed" "pass"

### section 3.5.1.3
### nftables {not installed,stopped,masked}
if rpm -q nftables >/dev/null ; then
	[[ $(systemctl status nftables \
			| grep "Active: " \
			| grep -v "active (running) ") == "" \
		&& $(systemctl is-enabled nftables) == "masked" ]] \
		&& score "3.5.1.3 Ensure nftables is not installed or stopped and masked" "pass" \
		|| score "3.5.1.3 Ensure nftables is not installed or stopped and masked" "fail"
else
	score "3.5.1.3 Ensure nftables is not installed or stopped and masked" "pass"
fi

### section 3.5.1.4
### firewalld enabled and running
#[[ $(systemctl is-enabled firewalld) == "enabled" \
#	&& $(sudo firewall-cmd --state) == "running" ]] \
#	&& score "3.5.1.4 Ensure firewalld service is enabled and running" "pass" \
#	|| score "3.5.1.4 Ensure firewalld service is enabled and running" "fail"
score "3.5.1.4 Ensure firewalld service is enabled and running" "excluded"

### section 3.5.1.5
### default zone set
#sudo firewall-cmd --get-default-zone
score "3.5.1.5 Ensure default zone is set" "excluded"

### section 3.5.1.6
### network interfaces assigned to appropriate zone
#nmcli -t connection show | aws -F: '{if($4){print $4}}' | while read INT; do firewall-cmd --get-active-zones | grep -B1 $INT; done
score "3.5.1.6 Ensure network interfaces are assigned to appropriate zone" "excluded"

### section 3.5.1.7
### unnecessary services and ports not accepted
#sudo firewall-cmd --get-active-zones | awk '!/:/ {print $1}' | while read ZN; do firewall-cmd --list-all --zone=$ZN; done
score "3.5.1.7 Ensure unnecessary services and ports are not accepted" "excluded"

### section 3.5.2
### config nftables
#script on page 240
#nft -t /etc/nftables/nftables.rules
score "3.5.2 Configure nftables" "excluded"

### section 3.5.2.1
### nftables installed
#rpm -q nftables >/dev/null \
#	&& score "3.5.2.1 Ensure nftables is installed" "pass" \
#	|| score "3.5.2.1 Ensure nftables is installed" "fail"
score "3.5.2.1 Ensure nftables is installed" "excluded"

### section 3.5.2.2
### firewalld not installed or stopped and masked
#if rpm -q firewalld >/dev/null ; then
#	[[ $(systemctl status firewalld \
#		| grep "Active: " \
#		| grep -v "active (running) ") == "" \
#		&& $(systemctl is-enabled firewalld) == "masked" ]] \
#	&& score "3.5.2.2 Ensure firewalld is not installed or stopped and masked" "pass" \
#	|| score "3.5.2.2 Ensure firewalld is not installed or stopped and masked" "fail"
#else
#	score "3.5.2.2 Ensure firewalld is not installed or stopped and masked" "pass"
#fi
score "3.5.2.2 Ensure firewalld is not installed or stopped and masked" "excluded"

### section 3.5.2.3
### iptables-services not installed
#rpm -q iptables-services >/dev/null \
#	&& score "3.5.2.3 Ensure iptables-services package is not installed" "fail" \
#	|| score "3.5.2.3 Ensure iptables-services package is not installed" "pass"
score "3.5.2.3 Ensure iptables-services package is not installed" "excluded"

### section 3.5.2.4
### flush iptables
score "3.5.2.4 Ensure iptables are flushed" "excluded"

#if $(which nft >/dev/null 2>&1) ; then
#	### section 3.5.2.5
#	### nft table exists
#	[[ $(nft list tables) != "" ]] \
#		&& score "3.5.2.5 Ensure a table exists" "pass" \
#		|| score "3.5.2.5 Ensure a table exists" "fail"
#	
#	### section 3.5.2.6
#	### base chains exist
#	[[ $(nft list ruleset | grep 'hook input') \
#		== "type filter hook input priority 0;" \
#		&& $(nft list ruleset | grep 'hook forward') \
#		== "type filter hook forward priority 0;" \
#		&& $(nft list ruleset | grep 'hook output') \
#		== "type filter hook output priority 0;" ]] \
#		&& score "3.5.2.6 Ensure base chains exists" "pass" \
#		|| score "3.5.2.6 Ensure base chains exists" "fail"
#	
#	### section 3.5.2.7
#	### loopback traffic configured
#	score "3.5.2.7 Ensure loopback traffic is configured" "ipv6"
#	
#	### section 3.5.2.8
#	### outbound and established connections config
#	score "3.5.2.8 Ensure outbound and established connections are configured" "manual"
#	
#	### section 3.5.2.9
#	### default deny firewall policy
#	[[ $(nft list ruleset | grep 'hook input') \
#		== "type filter hook input priority 0; policy drop;" \
#		&& $(nft list ruleset | grep 'hook forward') \
#		== "type filter hook forward priority 0; policy drop;" \
#		&& $(nft list ruleset | grep 'hook output') \
#		== "type filter hook output priority 0; policy drop;" ]] \
#		&& score "3.5.2.9 Ensure default deny firewall policy" "pass" \
#		|| score "3.5.2.9 Ensure default deny firewall policy" "fail"
#	
#	### section 3.5.2.10
#	### nftables service enabled
#	[[ $(systemctl is-enabled nftables) == "enabled" ]] \
#		&& score "3.5.2.10 Ensure nftables service is enabled" "pass" \
#		|| score "3.5.2.10 Ensure nftables service is enabled" "fail"
#	
#	### section 3.5.2.11
#	### nftables rules permanent
#	score "3.5.2.11 Ensure nftables are permanent" "manual"
#else
#	score "3.5.2.5 Ensure a table exists" "n/a"
#	score "3.5.2.6 Ensure base chains exists" "n/a"
#	score "3.5.2.7 Ensure loopback traffic is configured" "n/a"
#	score "3.5.2.8 Ensure outbound and established connections are configured" "n/a"
#	score "3.5.2.9 Ensure default deny firewall policy" "n/a"
#	score "3.5.2.10 Ensure nftables service is enabled" "n/a"
#	score "3.5.2.11 Ensure nftables are permanent" "n/a"
#fi
score "3.5.2.5 Ensure a table exists" "excluded"
score "3.5.2.6 Ensure base chains exists" "excluded"
score "3.5.2.7 Ensure loopback traffic is configured" "excluded"
score "3.5.2.8 Ensure outbound and established connections are configured" "excluded"
score "3.5.2.9 Ensure default deny firewall policy" "excluded"
score "3.5.2.10 Ensure nftables service is enabled" "excluded"
score "3.5.2.11 Ensure nftables are permanent" "excluded"

### section 3.5.3.1.1
### iptables installed
#rpm -q iptables iptables-services >/dev/null \
#	&& score "3.5.3.1.1 Ensure iptables packages are installed" "pass" \
#	|| score "3.5.3.1.1 Ensure iptables packages are installed" "fail"
score "3.5.3.1.1 Ensure iptables packages are installed" "excluded"

### section 3.5.3.1.2
### nftables not installed
#rpm -q nftables >/dev/null \
#	&& score "3.5.3.1.2 Ensure nftables packages are not installed" "fail" \
#	|| score "3.5.3.1.2 Ensure nftables packages are not installed" "pass"
score "3.5.3.1.2 Ensure nftables packages are not installed" "excluded"

### section 3.5.3.1.3
### firewalld not installed or stopped and masked
#if rpm -q firewalld >/dev/null ; then
#	[[ $(systemctl status firewalld \
#			| grep "Active: " \
#			| grep -v "active (running) ") == "" \
#		&& $(systemctl is-enabled firewalld) == "masked" ]] \
#	&& score "3.5.3.1.3 Ensure firewalld is not installed or stopped and masked" "pass" \
#	|| score "3.5.3.1.3 Ensure firewalld is not installed or stopped and masked" "fail"
#else
#	score "3.5.3.1.3 Ensure firewalld is not installed or stopped and masked" "pass"
#fi
score "3.5.3.1.3 Ensure firewalld is not installed or stopped and masked" "excluded"

### section 3.5.3.2
### config ipv4 iptables
score "3.5.3.2 Configure IPv4 iptables" "excluded"

### section 3.5.3.2.1
### default deny firewall policy
#[[ $(sudo iptables -L \
#	| grep -E "(INPUT|FORWARD|OUTPUT).*policy (DROP|REJECT)" \
#	| wc -l) -eq 3 ]] \
#	&& score "3.5.3.2.1 Ensure default deny firewall policy" "pass" \
#	|| score "3.5.3.2.1 Ensure default deny firewall policy" "fail"
score "3.5.3.2.1 Ensure default deny firewall policy" "excluded"

### section 3.5.3.2.2
### loopback traffic config
#input_score=0
#ouput_score=0
#input_rules='    0     0 ACCEPT     all  --  lo     *       0.0.0.0/0            0.0.0.0/0
#    0     0 DROP       all  --  lo     *       127.0.0.0/8          0.0.0.0/0'
#output_rules='    0     0 ACCEPT     all  --  *      lo      0.0.0.0/0            0.0.0.0/0'
#if sudo iptables -L INPUT -v -n | head -n 1 | grep "policy DROP" ; then
#	system_rules=$(iptables -L INPUT -v -n | tail -n+3)
#	[ "${input_rules}" = "${input_rules}" ] \
#		&& input_score=0 \
#		|| input_score=1
#else
#	input_score=1
#fi
#
#if sudo iptables -L OUTPUT -v -n | head -n 1 | grep "policy DROP" ; then
#	system_rules=$(iptables -L OUTPUT -v -n | tail -n+3)
#	[ "${output_rules}" = "${output_rules}" ] \
#		&& output_score=0 \
#		|| output_score=1
#else
#	output_score=1
#fi
#
#[[ ${input_score} == 0 && ${output_score} == 0 ]] \
#	&& score "3.5.3.2.2 Ensure loopback traffic is configured" "pass" \
#	|| score "3.5.3.2.2 Ensure loopback traffic is configured" "fail"
score "3.5.3.2.2 Ensure loopback traffic is configured" "excluded"

### section 3.5.3.2.3
### outbound and established connections config
score "3.5.3.2.3 Ensure outbound and established connections are configured" "excluded"

### section 3.5.3.2.4
### firewall rules exist for open ports
score "3.5.3.2.4 Ensure firewall rules exist for all open ports" "excluded"

### section 3.5.3.2.5
### iptables rules saved
score "3.5.3.2.5 Ensure iptables rules are saved" "excluded"

### section 3.5.3.2.6
### iptables enabled and running
#[[ $(systemctl is-enabled iptables >/dev/null 2>&1) == "enabled" \
#	&& $(systemctl status iptables >/dev/null 2>&1 \
#			| grep " Active: active (running) ") ]] \
#	&& score "3.5.3.2.6 Ensure iptables is enabled and running" "pass" \
#	|| score "3.5.3.2.6 Ensure iptables is enabled and running" "fail"
score "3.5.3.2.6 Ensure iptables is enabled and running" "excluded"

#if [[ ${ipv6} -eq 1 ]] ; then
#	### section 3.5.3.3
#	### config ipv6 iptables
#	score "3.5.3.3 Configure IPv6 ip6tables" "manual"
#
#	### section 3.5.3.3.1
#	### ipv6 default deny firewall policy
#	[[ $(sudo ip6tables -L \
#		| grep -E "(INPUT|FORWARD|OUTPUT).*policy (DROP|REJECT)" \
#		| wc -l) -eq 3 ]] \
#		&& score "3.5.3.3.1 Ensure IPv6 default deny firewall policy" "pass" \
#		|| score "3.5.3.3.1 Ensure IPv6 default deny firewall policy" "fail"
#
#	### section 3.5.3.3.2
#	### loopback traffic config
#	score "3.5.3.3.2 Ensure loopback traffic is configured" "manual"
#
#	### section 3.5.3.3.3
#	### outbound and established connections config
#	score "3.5.3.3.3 Ensure IPv6 outbound and established connections are configured" "manual"
#
#	### section 3.5.3.3.4
#	### firewall rules exist for open ports
#	score "3.5.3.3.4 Ensure IPv6 firewall rules exist for all open ports" "manual"
#
#	### section 3.5.3.3.5
#	### ip6tables rules saved
#	score "3.5.3.3.5 Ensure ip6tables rules are saved" "manual"
#
#	### section 3.5.3.3.6
#	### ip6tables enabled and running
#	[[ $(systemctl is-enabled ip6tables >/dev/null 2>&1) == "enabled" \
#		&& $(systemctl status ip6tables >/dev/null 2>&1 \
#				| grep " Active: active (running) ") ]] \
#		&& score "3.5.3.3.6 Ensure ip6tables is enabled and running" "pass" \
#		|| score "3.5.3.3.6 Ensure ip6tables is enabled and running" "fail"
#
#else
#	score "3.5.3.3 Configure IPv6 ip6tables" "n/a"
#	score "3.5.3.3.1 Ensure IPv6 default deny firewall policy" "n/a"
#	score "3.5.3.3.2 Ensure IPv6 loopback traffic is configured" "n/a"
#	score "3.5.3.3.3 Ensure IPv6 outbound and established connections are configured" "n/a"
#	score "3.5.3.3.4 Ensure IPv6 firewall rules exist for all open ports" "n/a"
#	score "3.5.3.3.5 Ensure ip6tables rules are saved" "n/a"
#	score "3.5.3.3.6 Ensure ip6tables is enabled and running" "n/a"
#fi
score "3.5.3.3 Configure IPv6 ip6tables" "excluded"
score "3.5.3.3.1 Ensure IPv6 default deny firewall policy" "excluded"
score "3.5.3.3.2 Ensure IPv6 loopback traffic is configured" "excluded"
score "3.5.3.3.3 Ensure IPv6 outbound and established connections are configured" "excluded"
score "3.5.3.3.4 Ensure IPv6 firewall rules exist for all open ports" "excluded"
score "3.5.3.3.5 Ensure ip6tables rules are saved" "excluded"
score "3.5.3.3.6 Ensure ip6tables is enabled and running" "excluded"

### section 4.1.1.1
### auditd installed
rpm -q audit audit-libs >/dev/null \
	&& score "4.1.1.1 Ensure auditd is installed" "pass" \
	|| score "4.1.1.1 Ensure auditd is installed" "fail"

### section 4.1.1.2
### auditd enabled and running
[[ $(systemctl is-enabled auditd) == "enabled" \
	&& $(systemctl status auditd | grep 'Active: active (running) ') ]] \
	&& score "4.1.1.2 Ensure auditd service is enabled and running" "pass" \
	|| score "4.1.1.2 Ensure auditd service is enabled and running" "fail"

### section 4.1.1.3
### auditing for proc start prior to auditd
sudo grep "^\s*linux" /boot/grub2/grub.cfg | grep -v "audit=1" >/dev/null \
	&& score "4.1.1.3 Ensure auditing for processes that start prior to auditd is enabled" "fail" \
	|| score "4.1.1.3 Ensure auditing for processes that start prior to auditd is enabled" "pass"

### section 4.1.2.1
### audit log storage
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo grep "^max_log_file\ =" /etc/audit/auditd.conf) == "max_log_file = 8" ]] \
		&& score "4.1.2.1 Ensure audit log storage size is configured" "pass" \
		|| score "4.1.2.1 Ensure audit log storage size is configured" "fail"
else
	score "4.1.2.1 Ensure audit log storage size is configured" "excluded"
fi

### section 4.1.2.2
### audit logs not deleted
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo grep max_log_file_action /etc/audit/auditd.conf) \
		== "max_log_file_action = keep_logs" ]] \
		&& score "4.1.2.2 Ensure audit logs are not automatically deleted" "pass" \
		|| score "4.1.2.2 Ensure audit logs are not automatically deleted" "fail"
else
	score "4.1.2.2 Ensure audit logs are not automatically deleted" "excluded"
fi

### section 4.1.2.3
### system disabled when audit logs full
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo grep space_left_action /etc/audit/auditd.conf) \
			== "space_left_action = email" \
		&& $(sudo grep action_mail_acct /etc/audit/auditd.conf) \
			== "action_mail_acct = root" \
		&& $(sudo grep admin_space_left_action /etc/audit/auditd.conf) \
			== "admin_space_left_action = halt" ]] \
		&& score "4.1.2.3 Ensure system is disabled when audit logs are full" "pass" \
		|| score "4.1.2.3 Ensure system is disabled when audit logs are full" "fail"
else
	score "4.1.2.3 Ensure system is disabled when audit logs are full" "excluded"
fi

### section 4.1.2.4
### sufficient audit_backlog_limit
if [[ "$dockerhost" -eq 0 ]] ; then
	[[ $(sudo grep "^\s*linux" /boot/grub2/grub.cfg \
			| grep -v "audit_backlog_limit=") == "" \
		&& $(sudo grep "audit_backlog_limit=" /boot/grub2/grub.cfg) \
			== "" ]] \
		&& score "4.1.2.4 Ensure audit_backlog_limit is sufficient" "pass" \
		|| score "4.1.2.4 Ensure audit_backlog_limit is sufficient" "fail"
else
	score "4.1.2.4 Ensure audit_backlog_limit is sufficient" "excluded"
fi

### section 4.1.3
### events modify date/time collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.3 Ensure events that modify date and time information are collected" "level2"
else
	score "4.1.3 Ensure events that modify date and time information are collected" "excluded"
fi

### section 4.1.4
### events modify user/group collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.4 Ensure events that modify user/group information are collected" "level2"
else
	score "4.1.4 Ensure events that modify user/group information are collected" "excluded"
fi

### section 4.1.5
### events modify network env collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.5 Ensure events that modify the system's network environment are collected" "level2"
else
	score "4.1.5 Ensure events that modify the system's network environment are collected" "excluded"
fi

### section 4.1.6
### events modify macs collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.6 Ensure events that modify the system's Mandatory Access Controls are collected" "level2"
else
	score "4.1.6 Ensure events that modify the system's Mandatory Access Controls are collected" "excluded"
fi

### section 4.1.7
### events login/logout collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.7 Ensure login and logout events are collected" "level2"
else
	score "4.1.7 Ensure login and logout events are collected" "excluded"
fi

### section 4.1.8
### events session initiation collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.8 Ensure session initiation information is collected" "level2"
else
	score "4.1.8 Ensure session initiation information is collected" "excluded"
fi

### section 4.1.9
### discretionary acp mod collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.9 Ensure discretionary access control permission modification events are collected" "level2"
else
	score "4.1.9 Ensure discretionary access control permission modification events are collected" "excluded"
fi

### section 4.1.10
### unsuccessful unauth file access collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.10 Ensure unsuccessful unauthorized file access attempts are collected" "level2"
else
	score "4.1.10 Ensure unsuccessful unauthorized file access attempts are collected" "excluded"
fi

### section 4.1.11
### priv commands collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.11 Ensure use of privileged commands is collected" "level2"
else
	score "4.1.11 Ensure use of privileged commands is collected" "excluded"
fi

### section 4.1.12
### successful fs mounts collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.12 Ensure successful file system mounts are collected" "level2"
else
	score "4.1.12 Ensure successful file system mounts are collected" "excluded"
fi

### section 4.1.13
### successful file deletion collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.13 Ensure file deletion events by users are collected" "level2"
else
	score "4.1.13 Ensure file deletion events by users are collected" "excluded"
fi

### section 4.1.14
### changes to sudoers collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.14 Ensure changes to system administration scope (sudoers) is collected" "level2"
else
	score "4.1.14 Ensure changes to system administration scope (sudoers) is collected" "excluded"
fi

### section 4.1.15
### sudolog collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.15 Ensure system administrator actions (sudolog) are collected" "level2"
else
	score "4.1.15 Ensure system administrator actions (sudolog) are collected" "excluded"
fi

### section 4.1.16
### kenel mod {un,}loading collected
if [[ "$dockerhost" -eq 0 ]] ; then
	score "4.1.16 Ensure kernel module loading and unloading is collected" "level2"
else
	score "4.1.16 Ensure kernel module loading and unloading is collected" "excluded"
fi

### section 4.1.17
### audit config immutable
score "4.1.17 Ensure the audit configuration is immutable" "level2"

### section 4.2.1.1
### rsyslog installed
rpm -q rsyslog >/dev/null \
	&& score "4.2.1.1 Ensure rsyslog is installed" "pass" \
	|| score "4.2.1.1 Ensure rsyslog is installed" "fail"

### section 4.2.1.2
### rsyslog enabled and running
if [[ $(systemctl is-enabled rsyslog) == "enabled" ]] \
	&& $(systemctl status rsyslog | \
		grep " Active: active (running) " >/dev/null 2>&1) ; then
	score "4.2.1.2 Ensure rsyslog service is enabled and running" "pass"
else
	score "4.2.1.2 Ensure rsyslog service is enabled and running" "fail"
fi

### section 4.2.1.3
### rsyslog default file permissions
#[[ $(sudo grep ^\$FileCreateMode \
#		/etc/rsyslog.conf \
#		/etc/rsyslog.d/*.conf) \
[[ $(sudo find /etc/rsyslog.conf /etc/rsyslog.d/*.conf -type f -exec \
	grep ^\$FileCreateMode {} \;) == '$FileCreateMode 0640' ]] \
	&& score "4.2.1.3 Ensure rsyslog default file permissions configured" "pass" \
	|| score "4.2.1.3 Ensure rsyslog default file permissions configured" "fail"

### section 4.2.1.4
### logging is configured
rsylog_compare_conf="$ModLoad imuxsock # provides support for local system logging (e.g. via logger command)
$ModLoad imjournal # provides access to the systemd journal
$WorkDirectory /var/lib/rsyslog
$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat
$IncludeConfig /etc/rsyslog.d/*.conf
$OmitLocalLogging on
$IMJournalStateFile imjournal.state
*.info;mail.none;authpriv.none;cron.none                /var/log/messages
authpriv.*                                              /var/log/secure
mail.*                                                  -/var/log/maillog
cron.*                                                  /var/log/cron
*.emerg                                                 :omusrmsg:*
uucp,news.crit                                          /var/log/spooler
local7.*                                                /var/log/boot.log"
score "4.2.1.4 Ensure logging is configured" "excluded"

### section 4.2.1.5
### rsyslog send logs to remote host
#[[ $(sudo grep "^*.*[^I][^I]*@" \
#		/etc/rsyslog.conf \
#		/etc/rsyslog.d/*.conf) \
#		== "*.* @@loghost.example.com" ]] \
#	&& score "4.2.1.5 Ensure rsyslog is configured to send logs to a remote host" "pass" \
#	|| score "4.2.1.5 Ensure rsyslog is configured to send logs to a remote host" "fail"
score "4.2.1.5 Ensure rsyslog is configured to send logs to a remote host" "excluded"

### section 4.2.1.6
### remote rsyslog msgs on designated hosts
#[[ $(sudo grep '$ModLoad imtcp' \
#		/etc/rsyslog.conf \
#		/etc/rsyslog.d/*.conf) \
#		&& $(sudo grep '$InputTCPServerRun' \
#				/etc/rsyslog.conf \
#				/etc/rsyslog.d/*.conf) \
#				== '$InputTCPServerRun 514' ]] \
#	&& score "4.2.1.6 Ensure remote rsyslog messages are only accepted on designated log hosts" "pass" \
#	|| score "4.2.1.6 Ensure remote rsyslog messages are only accepted on designated log hosts" "fail"
score "4.2.1.6 Ensure remote rsyslog messages are only accepted on designated log hosts" "excluded"

### section 4.2.2.1
### journald send logs to rsyslog
[[ $(sudo grep -E ^\s*ForwardToSyslog /etc/systemd/journald.conf) \
	== "ForwardToSyslog=yes" ]] \
	&& score "4.2.2.1 Ensure journald is configured to send logs to rsyslog" "pass" \
	|| score "4.2.2.1 Ensure journald is configured to send logs to rsyslog" "fail"

### section 4.2.2.2
### journald compress large logs
[[ $(sudo grep -E ^\s*Compress /etc/systemd/journald.conf) \
	== "Compress=yes" ]] \
	&& score "4.2.2.2 Ensure journald is configured to compress large log files" "pass" \
	|| score "4.2.2.2 Ensure journald is configured to compress large log files" "fail"

### section 4.2.2.3
### journald write logs to peristent disk
[[ $(sudo grep -E ^\s*Storage /etc/systemd/journald.conf) \
	== "Storage=persistent" ]] \
	&& score "4.2.2.3 Ensure journald is configured to write logfiles to persistent disk" "pass" \
	|| score "4.2.2.3 Ensure journald is configured to write logfiles to persistent disk" "fail"

### section 4.2.3
### permissins on logfiles config
sudo find /var/log/ -type f -prune /var/log/supervisor -perm /g+wx,o+rwx -exec ls -l {} \; >/dev/null \
	&& score "4.2.3 Ensure permissions on all logfiles are configured" "fail" \
	|| score "4.2.3 Ensure permissions on all logfiles are configured" "pass"

### section 4.2.4
### logrotate configured
score "4.2.4 Ensure logrotate is configured" "manual"

if rpm -q cronie >/dev/null ; then
	### section 5.1.1
	### crond enabled and running
	if [[ $(systemctl is-enabled crond) == "enabled" ]] \
		&& systemctl status crond | grep " Active: active (running) " >/dev/null 2>&1 ; then
			score "5.1.1 Ensure cron daemon is enabled and running" "pass"
	else
		score "5.1.1 Ensure cron daemon is enabled and running" "fail"
	fi

	### section 5.1.2
	### permissions on /etc/crontab config
	sudo stat /etc/crontab \
		| grep "Access: (0600/-rw-------)  Uid: (    0/    root)   Gid: (    0/    root)" \
		>/dev/null \
		&& score "5.1.2 Ensure permissions on /etc/crontab are configured" "pass" \
		|| score "5.1.2 Ensure permissions on /etc/crontab are configured" "fail"

	### section 5.1.3
	### permissions on /etc/cron.hourly config
	sudo stat /etc/cron.hourly \
		| grep "Access: (0700/drwx------)  Uid: (    0/    root)   Gid: (    0/    root)" \
		>/dev/null \
		&& score "5.1.3 Ensure permissions on /etc/cron.hourly are configured" "pass" \
		|| score "5.1.3 Ensure permissions on /etc/cron.hourly are configured" "fail"

	### section 5.1.4
	### permissions on /etc/cron.daily config
	sudo stat /etc/cron.daily \
		| grep "Access: (0700/drwx------)  Uid: (    0/    root)   Gid: (    0/    root)" \
		>/dev/null 2>&1 \
		&& score "5.1.4 Ensure permissions on /etc/cron.daily are configured" "pass" \
		|| score "5.1.4 Ensure permissions on /etc/cron.daily are configured" "fail"

	### section 5.1.5
	### permissions on /etc/cron.weekly config
	sudo stat /etc/cron.weekly \
		| grep "Access: (0700/drwx------)  Uid: (    0/    root)   Gid: (    0/    root)" \
		>/dev/null \
		&& score "5.1.5 Ensure permissions on /etc/cron.weekly are configured" "pass" \
		|| score "5.1.5 Ensure permissions on /etc/cron.weekly are configured" "fail"

	### section 5.1.6
	### permissions on /etc/cron.monthly config
	sudo stat /etc/cron.monthly \
		| grep "Access: (0700/drwx------)  Uid: (    0/    root)   Gid: (    0/    root)" \
		>/dev/null \
		&& score "5.1.6 Ensure permissions on /etc/cron.monthly are configured" "pass" \
		|| score "5.1.6 Ensure permissions on /etc/cron.monthly are configured" "fail"

	### section 5.1.7
	### permissions on /etc/cron.d config
	sudo stat /etc/cron.d \
		| grep "Access: (0700/drwx------)  Uid: (    0/    root)   Gid: (    0/    root)" \
		>/dev/null \
		&& score "5.1.7 Ensure permissions on /etc/cron.d are configured" "pass" \
		|| score "5.1.7 Ensure permissions on /etc/cron.d are configured" "fail"
	
	### section 5.1.8
	### cron restricted to auth users
	sudo stat /etc/cron.deny >/dev/null 2>&1 \
		&& score "5.1.8 Ensure cron is restricted to authorized users" "fail" \
		|| score "5.1.8 Ensure cron is restricted to authorized users" "pass"
else
	score "5.1.1 Ensure cron daemon is enabled and running" "n/a"
	score "5.1.2 Ensure permissions on /etc/crontab are configured" "n/a"
	score "5.1.3 Ensure permissions on /etc/cron.hourly are configured" "n/a"
	score "5.1.4 Ensure permissions on /etc/cron.daily are configured" "n/a"
	score "5.1.5 Ensure permissions on /etc/cron.weekly are configured" "n/a"
	score "5.1.6 Ensure permissions on /etc/cron.monthly are configured" "n/a"
	score "5.1.7 Ensure permissions on /etc/cron.d are configured" "n/a"
	score "5.1.8 Ensure cron is restricted to authorized users" "n/a"
fi

if rpm -q at >/dev/null ; then
	### section 5.1.9
	### at restricted to authorized users
	sudo stat /etc/at.deny >/dev/null 2>&1 \
		&& score "5.1.9 Ensure at is restricted to authorized users" "fail" \
		|| score "5.1.9 Ensure at is restricted to authorized users" "pass"
else
	score "5.1.9 Ensure at is restricted to authorized users" "n/a"
fi

### section 5.2.1
### permissons on sshd_config
sudo stat /etc/ssh/sshd_config \
	| grep "Access: (0600/-rw-------)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured" "pass" \
	|| score "5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured" "fail"

### section 5.2.2
### permissions on ssh priv host key files
num_of_priv_keys=$(find /etc/ssh -xdev -type f -name 'ssh_host_*_key' | wc -l)
num_of_sshkeys_perm=$(find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec stat {} \; | grep "^Access.*0640.*Uid:\ \(.*root\).*Gid:\ \(.*ssh_keys\)" | wc -l)
num_of_root_perm=$(find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec stat {} \; | grep "^Access.*0600.*Uid:\ \(.*root\).*Gid:\ \(.*root\)" | wc -l)
total_correct_perms=$(( num_of_sshkeys_perm + num_of_root_perm ))
[[ $total_correct_perms -eq $num_of_priv_keys ]] \
	&& score "5.2.2 Ensure permissions on SSH private host key files are configured" "pass" \
	|| score "5.2.2 Ensure permissions on SSH private host key files are configured" "fail"

### section 5.2.3
### permissions on ssh public host key files
num_of_pub_keys=$(find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' | wc -l)
num_with_correct_access=$(find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec stat {} \; | grep "^Access.*0644" | wc -l)
[[ $num_with_correct_access -eq $num_of_pub_keys ]] \
	&& score "5.2.3 Ensure permissions on SSH public host key files are configured" "pass" \
	|| score "5.2.3 Ensure permissions on SSH public host key files are configured" "fail"

### section 5.2.4
### ssh access limited
#[[ $(sudo /usr/sbin/sshd -T | grep -E '^\s*(allow|deny)(users|groups)\s+\S+') \
#	== "" ]] \
#	&& score "5.2.4 Ensure SSH access is limited" "fail" \
#	|| score "5.2.4 Ensure SSH access is limited" "pass"
score "5.2.4 Ensure SSH access is limited" "excluded"

### section 5.2.5
### appropriate ssh loglevel
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep loglevel) \
		== "VERBOSE" || "INFO" ]] \
	&& score "5.2.5 Ensure SSH LogLevel is appropriate" "pass" \
	|| score "5.2.5 Ensure SSH LogLevel is appropriate" "fail"

### section 5.2.6
### ssh x11 forwarding disabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep x11forwarding) \
		== "x11forwarding no" ]] \
	&& score "5.2.6 Ensure SSH X11 forwarding is disabled" "pass" \
	|| score "5.2.6 Ensure SSH X11 forwarding is disabled" "fail"

### section 5.2.7
### ssh maxauthtries
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep maxauthtries) \
		== "maxauthtries 4" ]] \
	&& score "5.2.7 Ensure SSH MaxAuthTries is set to 4 or less" "pass" \
	|| score "5.2.7 Ensure SSH MaxAuthTries is set to 4 or less" "fail"

### section 5.2.8
### ssh ignorerhosts enabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep ignorerhosts) \
		== "ignorerhosts yes" ]] \
	&& score "5.2.8 Ensure SSH IgnoreRhosts is enabled" "pass" \
	|| score "5.2.8 Ensure SSH IgnoreRhosts is enabled" "fail"

### section 5.2.9
### ssh hostbasedauthentication disabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep hostbasedauthentication) \
		== "hostbasedauthentication no" ]] \
	&& score "5.2.9 Ensure SSH HostbasedAuthentication is disabled" "pass" \
	|| score "5.2.9 Ensure SSH HostbasedAuthentication is disabled" "fail"

### section 5.2.10
### ssh permitrootlogin disabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep permitrootlogin) \
		== "permitrootlogin no" ]] \
	&& score "5.2.10 Ensure SSH root login is disabled" "pass" \
	|| score "5.2.10 Ensure SSH root login is disabled" "fail"

### section 5.2.11
### ssh permitempytpasswords disabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep permitemptypasswords) \
		== "permitemptypasswords no" ]] \
	&& score "5.2.11 Ensure SSH PermitEmptyPasswords is disabled" "pass" \
	|| score "5.2.11 Ensure SSH PermitEmptyPasswords is disabled" "fail"

### section 5.2.12
### ssh permituserenvironment disabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep permituserenvironment) \
		== "permituserenvironment no" ]] \
	&& score "5.2.12 Ensure SSH PermitUserEnvironment is disabled" "pass" \
	|| score "5.2.12 Ensure SSH PermitUserEnvironment is disabled" "fail"

### section 5.2.13
### ssh only strong ciphers
case $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
        | grep ciphers) in
	*"3des-cbc"*| \
		*"aes128-cbc"*| \
		*"aes192-cbc"*| \
		*"aes256-cbc"*| \
		*"arcfour"*| \
		*"arcfour128"*| \
		*"arcfour256"*| \
		*"blowfish-cbc"*| \
		*"cast128-cbc"*| \
		*"rijndael-cbc@lysator.liu.se"*) score "5.2.13 Ensure only strong Ciphers are used" "fail" ;;
	*) score "5.2.13 Ensure only strong Ciphers are used" "pass" ;;
esac

### section 5.2.14
### ssh only strong mac algorithms
case $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep -i "MACs") in
	*"hmac-md5"*| \
		*"hmac-md5-96"*| \
		*"hmac-ripemd160"*| \
		*"hmac-sha1"*| \
		*"hmac-sha1-96"*| \
		*"umac-64@openssh.com"*| \
		*"umac-128@openssh.com"*| \
		*"hmac-md5-etm@openssh.com"*| \
		*"hmac-md5-96-etm@openssh.com"*| \
		*"hmac-ripemd160-etm@openssh.com"*| \
		*"hmac-sha1-etm@openssh.com"*| \
		*"hmac-sha1-96-etm@openssh.com"*| \
		*"umac-64-etm@openssh.com"*| \
		*"umac-128-etm@openssh.com"*) score "5.2.14 Ensure only strong MAC algorithms are used" "fail" ;;
	*) score "5.2.14 Ensure SSH  strong MAC algorithms are used" "pass" ;;
esac

### section 5.2.15
### only strong key exchange algorithms
case $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep kexalgorithms) in
	*"diffie-hellman-group1-sha1"*| \
		"diffie-hellman-group14-sha1"*| \
		"diffie-hellman-group-exchange-sha1"*) score "5.2.15 Ensure only strong Key Exchange algorithms are used" "fail" ;;
	*) score "5.2.15 Ensure only strong Key Exchange algorithms are used" "pass" ;;
esac

### section 5.2.16
### ssh idle timeout interval
#[[ $(sudo /usr/sbin/sshd -T \
#		-C user=root \
#		-C host="$(hostname)" \
#		-C addr="$(grep $(hostname) \
#		/etc/hosts \
#		| awk '{print $1}')" \
#		| grep clientaliveinterval) \
#		== "clientaliveinterval 300" ]] \
#	&& [[ $(sudo /usr/sbin/sshd -T \
#		-C user=root \
#		-C host="$(hostname)" \
#		-C addr="$(grep $(hostname) \
#		/etc/hosts \
#		| awk '{print $1}')" \
#		| grep clientalivecountmax) \
#		== "clientalivecountmax 3" ]] \
#	&& score "5.2.16 Ensure SSH Idle Timeout Interval is configured" "pass" \
#	|| score "5.2.16 Ensure SSH Idle Timeout Interval is configured" "fail"
score "5.2.16 Ensure SSH Idle Timeout Interval is configured" "excluded"

### section 5.2.17
### ssh logingracetime
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep logingracetime) \
		== "logingracetime 60" ]] \
	&& score "5.2.17 Ensure SSH LoginGraceTime is set to one minute or less" "pass" \
	|| score "5.2.17 Ensure SSH LoginGraceTime is set to one minute or less" "fail"

### section 5.2.18
### ssh warning banner configured
if [[ $(hostname) == *"gps"* ]] ; then
	score "5.2.18 Ensure SSH warning banner is configured" "excluded"
else
	[[ $(sudo /usr/sbin/sshd -T \
			-C user=root \
			-C host="$(hostname)" \
			-C addr="$(grep $(hostname) /etc/hosts)" \
			| grep banner) \
			== "banner /etc/issue.net" ]] \
		&& score "5.2.18 Ensure SSH warning banner is configured" "pass" \
		|| score "5.2.18 Ensure SSH warning banner is configured" "fail"
fi

### section 5.2.19
### ssh pam enabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep usepam) \
		== "usepam yes" ]] \
	&& score "5.2.19 Ensure SSH PAM is enabled" "pass" \
	|| score "5.2.19 Ensure SSH PAM is enabled" "fail"

### section 5.2.20
### ssh allowtcpforwarding disabled
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep allowtcpforwarding) \
		== "allowtcpforwarding no" ]] \
	&& score "5.2.20 Ensure SSH AllowTcpForwarding is disabled" "pass" \
	|| score "5.2.20 Ensure SSH AllowTcpForwarding is disabled" "fail"

### section 5.2.21
### ssh maxstartups configured
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep maxstartups) \
		== "maxstartups 10:30:60" ]] \
	&& score "5.2.21 Ensure SSH MaxStartups is configured" "pass" \
	|| score "5.2.21 Ensure SSH MaxStartups is configured" "fail"

### section 5.2.22
### ssh maxsessions limited
[[ $(sudo /usr/sbin/sshd -T \
		-C user=root \
		-C host="$(hostname)" \
		-C addr="$(grep $(hostname) /etc/hosts)" \
		| grep maxsessions | awk '{print $2}') \
		-lt 10 ]] \
	&& score "5.2.22 Ensure SSH MaxSessions is limited" "pass" \
	|| score "5.2.22 Ensure SSH MaxSessions is limited" "fail"

### section 5.3.1
### password creation requirements

score "5.3.1 Ensure password creation requirements are configured" "excluded"

### section 5.3.2
### failed password attempts configu
score "5.3.2 Ensure lockout for failed password attempts is configured" "excluded"

### section 5.3.3
### password hashing algorithm is sha-512
#[[ $(sudo grep -E \
#	'^\s*password\s+(\S+\s+)+pam_unix\.so\s+(\S+\s*)*(\s+#.*)?$' \
#	/etc/pam.d/system-auth) \
#	== "password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok" \
#	&& $(sudo grep -E \
#	'^\s*password\s+(\S+\s+)+pam_unix\.so\s+(\S+\s*)*(\s+#.*)?$' \
#	/etc/pam.d/password-auth) \
#	== "password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok" ]] \
#	&& score "5.3.3 Ensure password hashing algorithm is SHA-512" "pass" \
#	|| score "5.3.3 Ensure password hashing algorithm is SHA-512" "fail"
score "5.3.3 Ensure password hashing algorithm is SHA-512" "excluded"

### section 5.3.4
### password reuse is limited
#[[ $(sudo grep -P \
#	'^\s*password\s+(requisite|required)\s+pam_pwhistory\.so\s+([^#]+\s+)*remember=([5-9]|[1-9][0-9]+)\b' \
#	/etc/pam.d/system-auth) \
#	== "password    required    pam_pwhistory.so remember=5" \
#	&& $(sudo grep -P \
#	'^\s*password\s+(requisite|required)\s+pam_pwhistory\.so\s+([^#]+\s+)*remember=([5-9]|[1-9][0-9]+)\b' \
#	/etc/pam.d/password-auth) \
#	== "password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok" ]] \
#	&& score "5.3.4 Ensure password reuse is limited" "pass" \
#	|| score "5.3.4 Ensure password reuse is limited" "fail"
score "5.3.4 Ensure password reuse is limited" "excluded"

### section 5.4.1.1
### password expiration 365 days
#[[ $(sudo grep ^\s*PASS_MAX_DAYS /etc/login.defs) \
#	== "PASS_MAX_DAYS 365" ]] \
#	&& score "5.4.1.1 Ensure password expiration is 365 days or less" "pass" \
#	|| score "5.4.1.1 Ensure password expiration is 365 days or less" "manual"
score "5.4.1.1 Ensure password expiration is 365 days or less" "excluded"

### section 5.4.1.2
### minimum days between password changes
#[[ $(sudo grep ^\s*PASS_MIN_DAYS /etc/login.defs) \
#	== "PASS_MIN_DAYS 1" ]] \
#	&& score "5.4.1.2 Ensure minimum days between password changes is configured" "pass" \
#	|| score "5.4.1.2 Ensure minimum days between password changes is configured" "manual"
score "5.4.1.2 Ensure minimum days between password changes is configured" "excluded"

### section 5.4.1.3
### password expiration warning 7 days
#[[ $(sudo grep ^\s*PASS_WARN_AGE /etc/login.defs) \
#	== "PASS_WARN_AGE 7" ]] \
#	&& score "5.4.1.3 Ensure password expiration warning days is 7 or more" "pass" \
#	|| score "5.4.1.3 Ensure password expiration warning days is 7 or more" "manual"
score "5.4.1.3 Ensure password expiration warning days is 7 or more" "excluded"

### section 5.4.1.4
### inactive password lock 30 days
#[[ $(sudo useradd -D | grep INACTIVE) \
#	== "INACTIVE=30" ]] \
#	&& score "5.4.1.4 Ensure inactive password lock is 30 days or less" "pass" \
#	|| score "5.4.1.4 Ensure inactive password lock is 30 days or less" "manual"
score "5.4.1.4 Ensure inactive password lock is 30 days or less" "excluded"

### section 5.4.1.5
### last passwd change date in past
#user_count=0
#for usr in $(sudo cut -d: -f1 /etc/shadow) ; do
#	[[ $(sudo chage --list $usr \
#		| grep '^Last password change' \
#		| cut -d: -f2) > $(date) ]] \
#	&& user_count=$(( ${user_count} + 1 ))
#done
#
#[[ ${user_count} -gt 0 ]] \
# && score "5.4.1.5 Ensure all users last password change date is in the past" "fail" \
# || score "5.4.1.5 Ensure all users last password change date is in the past" "pass"
score "5.4.1.5 Ensure all users last password change date is in the past" "excluded"

### section 5.4.2
### system accounts secure
[[ $(sudo awk -F: '($1!="root" && $1!="sync" \
			&& $1!="shutdown" \
			&& $1!="halt" \
			&& $1!~/^\+/ \
			&& $3<'"$(sudo awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' \
			&& $7!="/usr/sbin/nologin" \
			&& $7!="/sbin/nologin" \
			&& $7!="/bin/false") {print}' /etc/passwd | wc -l) -eq 0 ]] \
	&& [[ $(sudo awk -F: '($1!="root" \
			&& $1!~/^\+/ \
			&& $3<'"$(sudo awk '/^\s*UID_MIN/{print $2}' \
			/etc/login.defs)"') {print $1}' /etc/passwd \
			| sudo xargs -I '{}' passwd -S '{}' \
			| awk '($2!="L" && $2!="LK") {print $1}' | wc -l) -eq 0 ]] \
	&& score "5.4.2 Ensure system accounts are secured" "pass" \
	|| score "5.4.2 Ensure system accounts are secured" "fail"

### section 5.4.3
### default group for root gid0
[[ $(sudo grep "^root:" /etc/passwd | cut -f4 -d:) == 0 ]] \
	&& score "5.4.3 Ensure default group for the root account is GID 0" "pass" \
	|| score "5.4.3 Ensure default group for the root account is GID 0" "fail"

### section 5.4.4
### default user shell timeout config
#tmout=0
#for f in /etc/bashrc /etc/profile /etc/profile.d/*.sh ; do
#	sudo grep -Eq '(^|^[^#]*;)\s*(readonly|export(\s+[^$#;]+\s*)*)?\s*TMOUT=(900|[1-8][0-9][0-9]|[1-9][0-9]|[1-9])\b' $f \
#	&& sudo grep -Eq '(^|^[^#]*;)\s*readonly\s+TMOUT\b' $f \
#	&& sudo grep -Eq '(^|^[^#]*;)\s*export\s+([^$#;]+\s+)*TMOUT\b' $f \
#	&& tmout=$(( ${tmout} + 1 ))
#done
#
#[[ $(sudo grep -P '^\s*([^$#;]+\s+)*TMOUT=(9|[0-9][1-9]|0+|[1-9]\d{3,})\b\s*(\S+\s*)*(\s+#.*)?$' \
#		/etc/profile \
#		/etc/profile.d/*.sh \
#		/etc/bashrc) \
#	== "" ]] \
#	&& tmout=$(( ${tmout} + 1 ))
#
#[[ ${tmout} -eq 4 ]] \
#	&& score "5.4.4 Ensure default user shell timeout is configured" "pass" \
#	|| score "5.4.4 Ensure default user shell timeout is configured" "fail"
score "5.4.4 Ensure default user shell timeout is configured" "excluded"

### section 5.4.5
### default user umask config
[[ $(sudo grep -Ev '^\s*umask\s+\s*(0[0-7][2-7]7|[0-7][2-7]7|u=(r?|w?|x?)(r?|w?|x?)(r?|w?|x?),g=(r?x?|x?r?),o=)\s*(\s*#.*)?$' \
		/etc/profile \
		/etc/profile.d/*.sh \
		/etc/bashrc \
		| sudo grep -E '(^|^[^#]*)umask') \
	== "" \
	&& $(sudo grep -E '^\s*umask\s+\s*(0[0-7][2-7]7|[0-7][2-7]7|u=(r?|w?|x?)(r?|w?|x?)(r?|w?|x?),g=(r?x?|x?r?),o=)\s*(\s*#.*)?$' \
		/etc/profile \
		/etc/profile.d/*.sh \
		/etc/bashrc \
		| wc -l) -ge 1 ]] \
	&& score "5.4.5 Ensure default user umask is configured" "pass" \
	|| score "5.4.5 Ensure default user umask is configured" "fail"

### section 5.5
### root login restricted
score "5.5 Ensure root login restricted to system console" "manual"

### section 5.6
### su command restricted
#su_group=$(sudo grep -E '^\s*auth\s+required\s+pam_wheel\.so\s+(\S+\s+)*use_uid\s+(\S+\s+)*group=\S+\s*(\S+\s*)*(\s+#.*)?$' \
#			/etc/pam.d/su | awk -F= '{print $2}')
#
#[[ $(sudo grep ${su_group} /etc/group | awk -F\: '{print $NF}') == "" ]] \
#	&& score "5.6 Ensure access to the su command is restricted" "pass" \
#	|| score "5.6 Ensure access to the su command is restricted" "fail"
score "5.6 Ensure access to the su command is restricted" "excluded"

### section 6.1.1
### audit sys file perms
score "6.1.1 Audit system file permissions" "manual"

### section 6.1.2
### perms on /etc/passwd config
sudo stat /etc/passwd \
	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.2 Ensure permissions on /etc/passwd are configured" "pass" \
	|| score "6.1.2 Ensure permissions on /etc/passwd are configured" "fail"

### section 6.1.3
### perms on /etc/shadow config
sudo stat /etc/shadow \
	| grep "Access: (0000/----------)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.3 Ensure permissions on /etc/shadow are configured" "pass" \
	|| score "6.1.3 Ensure permissions on /etc/shadow are configured" "fail"

### section 6.1.4
### perms on /etc/group config
sudo stat /etc/group \
	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.4 Ensure permissions on /etc/group are configured" "pass" \
	|| score "6.1.4 Ensure permissions on /etc/group are configured" "fail"

### section 6.1.5
### perms on /etc/gshadow config
sudo stat /etc/gshadow \
	| grep "Access: (0000/----------)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.5 Ensure permissions on /etc/gshadow are configured" "pass" \
	|| score "6.1.5 Ensure permissions on /etc/gshadow are configured" "fail"

### section 6.1.6
### perms on /etc/passwd- config
sudo stat /etc/passwd- \
	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.6 Ensure permissions on /etc/passwd- are configured" "pass" \
	|| score "6.1.6 Ensure permissions on /etc/passwd- are configured" "fail"

### section 6.1.7
### perms on /etc/shadow- config
sudo stat /etc/shadow- \
	| grep "Access: (0000/----------)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.7 Ensure permissions on /etc/shadow- are configured" "pass" \
	|| score "6.1.7 Ensure permissions on /etc/shadow- are configured" "fail"

### section 6.1.8
### perms on /etc/group- config
sudo stat /etc/group- \
	| grep "Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.8 Ensure permissions on /etc/group- are configured" "pass" \
	|| score "6.1.8 Ensure permissions on /etc/group- are configured" "fail"

### section 6.1.9
### perms on /etc/gshadow- config
sudo stat /etc/gshadow- \
	| grep "Access: (0000/----------)  Uid: (    0/    root)   Gid: (    0/    root)" \
	>/dev/null \
	&& score "6.1.9 Ensure permissions on /etc/gshadow- are configured" "pass" \
	|| score "6.1.9 Ensure permissions on /etc/gshadow- are configured" "fail"

### section 6.1.10
### no world writable files
[[ $(sudo df --local -P \
	| grep -v -e "${ignorelocal}" \
	| awk '{if (NR!=1) print $6}' \
	| sudo xargs -I '{}' find '{}' -xdev -type f -perm -0002) \
	== "" ]] \
	&& score "6.1.10 Ensure no world writable files exist (local only)" "pass" \
	|| score "6.1.10 Ensure no world writable files exist (local only)" "fail"

### section 6.1.11
### no unowned files or dirs
[[ $(sudo df --local -P \
	| grep -v -e "${ignorelocal}" \
	| awk '{if (NR!=1) print $6}' \
	| sudo xargs -I '{}' find '{}' -xdev -nouser) \
	== "" ]] \
	&& score "6.1.11 Ensure no unowned files or directories exist (local only)" "pass" \
	|| score "6.1.11 Ensure no unowned files or directories exist (local only)" "fail"

### section 6.1.12
### no ungrouped files or dirs
[[ $(sudo df --local -P \
	| grep -v -e "${ignorelocal}" \
	| awk '{if (NR!=1) print $6}' \
	| sudo xargs -I '{}' find '{}' -xdev -nogroup) \
	== "" ]] \
	&& score "6.1.12 Ensure no ungrouped files or directories exist (local only)" "pass" \
	|| score "6.1.12 Ensure no ungrouped files or directories exist (local only)" "fail"

### section 6.1.13
### audit suid exe
[[ $(sudo df --local -P \
	| grep -v -e "${ignorelocal}" \
	| awk '{if (NR!=1) print $6}' \
	| sudo xargs -I '{}' find '{}' -xdev -type f -perm -4000) \
	== "" ]] \
	&& score "6.1.13 Audit SUID executables (local only)" "pass" \
	|| score "6.1.13 Audit SUID executables (local only)" "manual"

### section 6.1.14
### audit sgid exe
[[ $(sudo df --local -P \
	| grep -v -e "${ignorelocal}" \
	| awk '{if (NR!=1) print $6}' \
	| sudo xargs -I '{}' find '{}' -xdev -type f -perm -2000) \
	== "" ]] \
	&& score "6.1.14 Audit SGID executables (local only)" "pass" \
	|| score "6.1.14 Audit SGID executables (local only)" "manual"

### section 6.2.1
### accounts use shadowed passwords
[[ $(sudo awk -F: '($2 != "x") { print $1 }' /etc/passwd) \
	== "" ]] \
	&& score "6.2.1 Ensure accounts in /etc/passwd use shadowed passwords" "pass" \
	|| score "6.2.1 Ensure accounts in /etc/passwd use shadowed passwords" "fail"

### section 6.2.2
### password fields not empty
[[ $(sudo awk -F: '($2 == "") { print $1 }' /etc/shadow) \
	== "" ]] \
	&& score "6.2.2 Ensure /etc/shadow password fields are not empty" "pass" \
	|| score "6.2.2 Ensure /etc/shadow password fields are not empty" "fail"

### section 6.2.3
### root is only uid 0
[[ $(sudo awk -F: '($3 == 0) { print $1 }' /etc/passwd) \
	== "root" ]] \
	&& score "6.2.3 Ensure root is the only UID 0 account" "pass" \
	|| score "6.2.3 Ensure root is the only UID 0 account" "fail"

### section 6.2.4
### root path integrity
path_err_count=0
# get root's path
root_path=$(sudo -Hiu root env | grep ^PATH | awk -F= '{print $2}')

# ensure no empty directory
echo "${root_path}" | grep -q "::" && path_err_count=$(( path_err_count + 1 ))

# ensure no trailing ':'
echo "${root_path}" | grep -q ":$" && path_err_count=$(( path_err_count + 1 ))

for x in $(echo "${root_path}" | tr ":" " ") ; do
	if [ -d "${x}" ] ; then

		# check if $PATH contains current working directory
		[ $(ls -ldH ${x} | awk '{print $9}') == "." ] && \
			path_err_count=$(( path_err_count + 1 ))

		# check that directories are owned by root
		[ $(ls -ldH ${x} | awk '{print $3}') != "root" ] && \
			path_err_count=$(( path_err_count + 1 ))
			
		# check if directory is group writable
		[ $(ls -ldH ${x} | awk '{print substr($1,6,1)}') != "-" ] && \
			path_err_count=$(( path_err_count + 1 ))

		# check if directory is world writable
		[ $(ls -ldH ${x} | awk '{print substr($1,9,1)}') != "-" ] && \
			path_err_count=$(( path_err_count + 1 ))

	elif [ -f "${x}" ] ; then
		# if not a directory
		path_err_count=$(( path_err_count + 1 ))
	fi
done
[[ $path_err_count -eq 0 ]] \
	&& score "6.2.4 Ensure root PATH Integrity" "pass" \
	|| score "6.2.4 Ensure root PATH Integrity" "fail"

### section 6.2.5
### users' home dir exist
score "6.2.5 Ensure all users' home directories exist" "excluded"

### section 6.2.6
### users' home dir perms
score "6.2.6 Ensure users' home directories permissions are 750 or more restrictive" "excluded"

### section 6.2.7
### users own home dir
score "6.2.7 Ensure users own their home directories" "excluded"

### section 6.2.8
### users' dot file not group/world writable
score "6.2.8 Ensure users' dot files are not group or world writable" "excluded"

### section 6.2.9
### no users have .forward
score "6.2.9 Ensure no users have .forward files" "excluded"

### section 6.2.10
### no users have .netrc
score "6.2.10 Ensure no users have .netrc files" "excluded"

### section 6.2.11
### users' .netrc not group/world accessible
score "6.2.11 Ensure users' .netrc files are not group or world accessible" "excluded"

### section 6.2.12
### no users have .rhosts
score "6.2.12 Ensure no users have .rhosts files" "excluded"

### section 6.2.13
### groups in /etc/passwd exist in /etc/group
missing_group_count=0
for i in $(cut -s -d: -f4 /etc/passwd | sort -u >/dev/null) ; do
	grep -q -P "^.*?:[^:]*:$i:" /etc/group >/dev/null
	if [ $? -ne 0 ] ; then
		missing_group_count=$(( missing_group_count + 1 ))
	fi
done
[[ $missing_group_count -eq 0 ]] \
	&& score "6.2.13 Ensure all groups in /etc/passwd exist in /etc/group" "pass" \
	|| score "6.2.13 Ensure all groups in /etc/passwd exist in /etc/group" "fail"

### section 6.2.14
### no duplicate uids
score "6.2.14 Ensure no duplicate UIDs exist" "excluded"

### section 6.2.15
### no duplicate gids
score "6.2.15 Ensure no duplicate GIDs exist" "excluded"

### section 6.2.16
### no duplicate user names
score "6.2.16 Ensure no duplicate user names exist" "excluded"

### section 6.2.17
### no duplicate group names
score "6.2.17 Ensure no duplicate group names exist" "excluded"

### section 6.2.18
### shadow group is empty
[[ $(sudo grep ^shadow:[^:]*:[^:]*:[^:]+ /etc/group) \
	== "" \
	&& $(sudo awk -F: '($4 == "<shadow-gid>") { print }' /etc/passwd) \
	== "" ]] \
	&& score "6.2.18 Ensure shadow group is empty" "pass" \
	|| score "6.2.18 Ensure shadow group is empty" "fail"

# end
#passed=$(grep "pass" ${results_doc} | wc -l)
#failed=$(grep "fail" ${results_doc} | wc -l)
#printf "\nTOTALS" >> ${results_doc}
#echo -e "${passed} | ${green}passed${reset}" >> ${results_doc}
#echo -e "${failed} | ${red}failed${reset}" >> ${results_doc}

column -s '|' -t "${results_doc}" > ${results_doc/.csv}.table
